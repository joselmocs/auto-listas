
(function ($) {
    var parsers = {
        none: function () {
            return [];
        },
        parse_language_predictions: function (json) {
            var parsed = [],
                item, i;
            for (i = 0; i < json.length; i++) {
                item = json[i];
                parsed.push({
                    data: [item],
                    value: item,
                    result: item,
                    index: i
                });
            }
            return parsed;
        },
        parse_predictions: function (json) {
            var parsed = [], item, i;
            if (json == undefined) {
                return parsed;
            }
            for (i = 0; i < json.matches.length; i++) {
                item = json.matches[i];
                parsed.push({
                    item: item,
                    data: [item.matchstring],
                    value: item.matchstring,
                    result: item.matchstring,
                    index: i
                });
            }
            return parsed;
        },
        parse_geo_aware_predictions: function (json) {
            var parsed = [],
                item, i;
            for (i = 0; i < json.response.docs.length; i++) {
                item = json.response.docs[i];
                item_address = ( !! item.address_text) ? item.address_text : null;
                item_postal = ( !! item.postal_city) ? item.postal_city : null;
                item_ypid = ( !! item.ypid) ? item.ypid : null;
                if (item.distance < 0.1) {
                    item_distance = "< 0.1";
                } else {
                    item_distance = ( !! item.distance) ? Number(item.distance).toFixed(1) : null;
                }
                parsed.push({
                    data: [item.suggestion],
                    value: item.suggestion,
                    result: item.suggestion,
                    address: item_address,
                    postal_city: item_postal,
                    ypid: item_ypid,
                    distance: item_distance,
                    index: i
                });
            }
            return parsed;
        }
    };
    var bucket = "";
    var lastCachedResult = null;
    var typedTerm = "";
    var parsedDataValues = function () {
        var parsedValues = [];
        if (lastCachedResult) {
            parsedValues = $.map(lastCachedResult, function (parsed_data) {
                if (parsed_data.item.type == 't') {
                    $("input[name='tmatchid']").val(parsed_data.item.matchid);
                }
                if (parsed_data.item.type == 'c' || parsed_data.item.type == 'd') {
                    $("input[name='lmatchtype']").val(parsed_data.item.type);
                    $("input[name='lmatchid']").val(parsed_data.item.matchid);
                }
                return parsed_data.value;
            });
        }
        return parsedValues;
    };
    var autoSuggestState = function (ui) {
        var selectedTerm = ui.item.value;
        var suggestSet = parsedDataValues();
        var selectedTermIndex = $.inArray(selectedTerm, suggestSet) + 1;
        var suggestTrackType = $('#search-terms').attr('data-suggest_type');
        var track_id = ( !! ui.item.ypid) ? 448 : 1789;
        var suggestState = {
            typedTerm: typedTerm,
            selectedTerm: selectedTerm,
            suggestSet: suggestSet,
            selectedTermIndex: selectedTermIndex,
            bucket: bucket,
            trackId: track_id,
            suggestTrackType: suggestTrackType
        };
        return suggestState;
    };
    $.fn.alAutocomplete = function (options) {
        var config = $.extend({}, $.fn.alAutocomplete.defaults, options);
        var $this = $(this);
        if (config.source == "") {
            return $this;
        }
        return $this.each(function (i, element) {
            var $element = $(element);
            var $ac = $element.autocomplete({
                select: function (event, ui) {
                    event.stopImmediatePropagation();
                    $element.trigger("auto_suggest_selected", [autoSuggestState(ui)]);
                },
                source: ((typeof config.source == "string") ? function (request, response) {
                    var term = request.term;
                    if (typeof $.fn.alAutocomplete.cache.lastXhr['abort'] != "undefined") {
                        $.fn.alAutocomplete.cache.lastXhr.abort();
                    }
                    var getJSONParams = $.extend({
                        q: function () {
                            return ($element.val());
                        },
                        g: $("#search-location").attr("value"),
                        timestamp: + new Date(),
                        ss: function () {
                            return ($element.val());
                        }
                    }, config.extraParams);
                    $.fn.alAutocomplete.cache.lastXhr = $.getJSON(config.source, getJSONParams, function (data, status, xhr) {
                        if (data != undefined) {
                            var parData = parsers[config.parser] ? parsers[config.parser](data) : [];
                            if (xhr === $.fn.alAutocomplete.cache.lastXhr || config.mode === "test") {
                                response(parData);
                            }
                            bucket = data.response ? data.response.sgt : "";
                            typedTerm = getJSONParams.q();
                            lastCachedResult = parData;
                        }
                    });
                } : config.source),
                delay: 100,
                minLength: 1
            });
            if ($ac.data("autocomplete")) {
                $ac.data("autocomplete")._renderItem = function (ul, item) {
                    var regEx = new RegExp("(" + $element.val() + ")", "gi");
                    return $('<li></li>').data("item.autocomplete", item).html('<a>' + item.value.replace(regEx, "<strong>$1</strong>") + '</a>').appendTo(ul);
                };
                $ac.data("autocomplete")._renderMenu = function (ul, items) {
                    var self = this;
                    if (config.resultHeader !== "") {
                        ul.append('<li class="ui-autocomplete-category">' + config.resultHeader + '</li>');
                    }
                    $.each(items, function (index, item) {
                        self._renderItem(ul, item);
                    });
                    $element.removeClass("ui-autocomplete-loading");
                };
            }
        });
    };
    $.fn.ypHypersuggest = function (options) {
        var config = $.extend({}, $.fn.alAutocomplete.defaults, options);
        var $this = $(this);
        if (config.source == "") {
            return $this;
        }
        return $this.each(function (i, element) {
            var $element = $(element);
            var $ac = $element.autocomplete({
                select: function (event, ui) {
                    event.stopImmediatePropagation();
                    $element.trigger("auto_suggest_selected", [autoSuggestState(ui)]);
                    if (ui.item.ypid) {
                        $element.trigger("hyper_suggest_MIP", [ui]);
                    } else {
                        $element.trigger("hyper_suggest_SRP", [ui]);
                    }
                },
                source: ((typeof config.source == "string") ? function (request, response) {
                    var term = request.term;
                    if (typeof $.fn.alAutocomplete.cache.lastXhr['abort'] != "undefined") {
                        $.fn.alAutocomplete.cache.lastXhr.abort();
                    }
                    var getJSONParams = $.extend({
                        hyperSuggest: true,
                        q: function () {
                            return ($element.val());
                        },
                        g: $("#search-location").attr("value"),
                        timestamp: +new Date(),
                        ss: function () {
                            return ($element.val());
                        }
                    }, config.extraParams);
                    $.fn.alAutocomplete.cache.lastXhr = $.getJSON(config.source, getJSONParams, function (data, status, xhr) {
                        var parData = parsers[config.parser] ? parsers[config.parser](data) : [];
                        if (xhr === $.fn.alAutocomplete.cache.lastXhr || config.mode === "test") {
                            response(parData);
                        }
                        bucket = data.response ? data.response.sgt : "";
                        typedTerm = getJSONParams.q();
                        lastCachedResult = parData;
                    });
                } : config.source),
                delay: 100,
                minLength: 1
            });
            if ($ac.data("autocomplete")) {
                $ac.data("autocomplete")._renderItem = function (ul, item) {
                    var regEx = new RegExp("(" + $element.val() + ")", "gi");
                    var suggestResult = item.value.replace(regEx, "<strong>$1</strong>");
                    if ((item.address) && (item.postal_city)) {
                        suggestResult += '<div>' + item.address + ', ' + item.postal_city + '</div>';
                    }
                    if (item.distance) {
                        suggestResult += '<span>' + item.distance + '</span>';
                    }
                    return $('<li></li>').data("item.autocomplete", item).html('<a>' + suggestResult + '</a>').appendTo(ul);
                };
                $ac.data("autocomplete")._renderMenu = function (ul, items) {
                    var self = this;
                    if (config.resultHeader !== "") {
                        ul.append('<li class="ui-autocomplete-category">' + config.resultHeader + '</li>');
                    }
                    $.each(items, function (index, item) {
                        self._renderItem(ul, item);
                    });
                    $element.removeClass("ui-autocomplete-loading");
                };
            }
        });
    };
    $.fn.alAutocomplete.defaults = {
        source: "",
        parser: "none",
        resultHeader: "",
        mode: "production",
        extraParams: {}
    };
    $.fn.alAutocomplete.cache = {
        lastXhr: {}
    };
})(jQuery);