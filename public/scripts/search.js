

var Search = {

    baseUrl: '/',

    initialize: function()
    {
        this.binds();
    },

    binds: function()
    {
        $('#search-terms').change(function() {
            $("input[name='tmatchid']").val("");
        }).alAutocomplete({
            source: "/proxy/search/terms",
            parser: "parse_predictions"
        });

        $('#search-location').change(function() {
            $("input[name='lmatchtype']").val("");
            $("input[name='lmatchid']").val("");
        }).alAutocomplete({
            source:"/proxy/search/location",
            parser:"parse_predictions",
            resultHeader:"Digite um <strong>Bairro</strong> ou <strong>Cidade</strong>"
        });
    }
};

$(document).ready(function() {
    Search.initialize();
});
