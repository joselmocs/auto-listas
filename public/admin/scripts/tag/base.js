

var Tag = {

    actual_element: null,
    editing: null,

    init: function() {
        this.binds();
    },

    binds: function()
    {
        $("#button_add_tag").click(function() {
            Tag.insert();
        });

        $("#button_edit_tag").click(function() {
            Tag.edit();
        });

        $("#button_mode_new").click(function() {
            Tag.mode.new();
        });

    },

    loading: {
        show: function() {
            $("#loading_overlay, #loading_overlay div").show();
        },
        hide: function() {
            $("#loading_overlay, #loading_overlay div").hide();
        }
    },

    mode:
    {
        new: function()
        {
            $("#error_add_tag").hide();
            $("span[name=msg_add_tag]").html("");

            if (Tag.actual_element != null) {
                Tag.actual_element.text("Editar");
                Tag.actual_element = null;
            }
            Tag.editing = null;

            $("#tag_field").val("");
            $("div[name=mode_edit]").hide();
            $("div[name=mode_new]").show();
        },

        edit: function(nome, id, element)
        {
            $("#error_add_tag").hide();
            $("span[name=msg_add_tag]").html("");

            if (Tag.actual_element != null) {
                Tag.actual_element.text("Editar");
            }
            Tag.actual_element = element;
            Tag.actual_element.text("Editando");
            Tag.editing = id;

            $("#tag_field").val(nome);
            $("div[name=mode_new]").hide();
            $("div[name=mode_edit]").show();

            $("div[name=mode_edit].section").show();
            $("div[name=mode_edit].section").find('h2').html(nome);
        }
    },

    remove: function()
    {
        if (Tag.editing != null) {
            window.location = "/padmin/business/tag/remove/"+ Tag.editing;
        }
    },

    edit: function()
    {
        $("#error_add_tag").hide();
        $("span[name=msg_add_tag]").html("");

        if (!$("#tag_field").val().length) {
            $("span[name=msg_add_tag]").html("Por favor, preencha o nome da etiqueta.");
            $("#error_add_tag").toggle(200);
            return;
        }

        Tag.loading.show();

        $.ajax({
            type: "POST",
            url: '/padmin/business/tag/edit',
            data: {
                'id': Tag.editing,
                'tag': $("#tag_field").val()
            },
            dataType: 'json',
            error: function() {
                Tag.loading.hide();
            },
            success: function(response) {
                if ('success' in response) {
                    window.location = '';
                }
                else {
                    Tag.loading.hide();
                    var erro = null;
                    $.each(response, function(x, field) {
                        $.each(field, function(y, msg) {
                            erro = msg;
                        });
                    });

                    if (erro) {
                        $("span[name=msg_add_tag]").html(erro);
                        $("#error_add_tag").toggle(200);
                    }
                }
            }
        });
    },

    insert: function()
    {
        $("#error_add_tag").hide();
        $("span[name=msg_add_tag]").html("");

        if (!$("#tag_field").val().length) {
            $("span[name=msg_add_tag]").html("Por favor, preencha o nome da etiqueta.");
            $("#error_add_tag").toggle(200);
            return;
        }

        Tag.loading.show();

        $.ajax({
            type: "POST",
            url: '/padmin/business/tag/add',
            data: {
                'tag': $("#tag_field").val()
            },
            dataType: 'json',
            error: function() {
                Tag.loading.hide();
            },
            success: function(response) {
                if ('success' in response) {
                    window.location = '';
                }
                else {
                    Tag.loading.hide();
                    var erro = null;
                    $.each(response, function(x, field) {
                        $.each(field, function(y, msg) {
                            erro = msg;
                        });
                    });

                    if (erro) {
                        $("span[name=msg_add_tag]").html(erro);
                        $("#error_add_tag").toggle(200);
                    }
                }
            }
        });
    }
};

$(document).ready(function() {
    Tag.init();
});