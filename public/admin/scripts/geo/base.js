
var Geo = {

    cache_cities: {},
    active_letter: null,
    active_uf: null,

    init: function() {
        this.binds();

        if (_context.estado != undefined) {
            this.active_uf = _context.estado;
            this.city.find(false);
        }
    },

    binds: function()
    {
        $(".select3").select3();

        $("#button_select_estado").click(function() {
            window.location = "/padmin/geo/city/" + $("#select_estado").val();
        });

        $("#selecionar_outro_estado").click(function() {
            window.location = "/padmin/geo/city";
        });

        $("input[name=filter]").click(function() {
            Geo.city.find(false);
        });
    },

    city:
    {
        find: function(refresh)
        {
            var letter = $("input[name=filter]:checked").val();

            this.active_letter = letter;

            if (!(letter in Geo.cache_cities) || refresh) {
                Geo.loading.show();
                $("#table_"+ letter).remove();
                $.ajax({
                    type: "POST",
                    url: '',
                    data: {
                        'letter': letter
                    },
                    dataType: 'json',
                    error: function() {
                        Geo.loading.hide();
                    },
                    success: function(response) {
                        Geo.process_table(letter, response);
                        Geo.cache_cities[letter] = true;
                        Geo.loading.hide();
                    }
                });
            }
            Geo.show_table(letter);
        },

        activate: function(city)
        {
            Geo.loading.show();
            $.ajax({
                type: "POST",
                url: '/padmin/geo/city/active/'+ city,
                data: {},
                dataType: 'json',
                error: function() {
                    Geo.loading.hide();
                },
                success: function() {
                    Geo.loading.hide();
                    Geo.city.find(true);
                }
            });
        },

        deactivate: function(city)
        {
            Geo.loading.show();
            $.ajax({
                type: "POST",
                url: '/padmin/geo/city/deactive/'+ city,
                data: {},
                dataType: 'json',
                error: function() {
                    Geo.loading.hide();
                },
                success: function() {
                    Geo.loading.hide();
                    Geo.city.find(true);
                }
            });
        }

    },

    loading: {
        show: function() {
            $("#loading_overlay, #loading_overlay div").show();
        },
        hide: function() {
            $("#loading_overlay, #loading_overlay div").hide();
        }
    },

    show_table: function(active_letter)
    {
        $.each(Geo.cache_cities, function(letter, i) {
            $("#table_"+ letter).hide();
        });
        $("#table_"+ active_letter).show();
    },

    process_table: function(letter, cities)
    {
        var template = "";
        $.each(cities, function(i, city) {
            var cidades = $("#template_cities").clone();
            cidades = cidades.html()
                .replace("{cidade}", city.name)
                .replace("{slug}", city.slug)
                .replace("{empresas}", city.qtde_empresas);

            if (city.active) {
                cidades = cidades.replace("{status}", "<span class='status status-important'><a href='javascript: Geo.city.deactivate("+ city.id +");'>Desativar</a></span>");
            }
            else {
                cidades = cidades.replace("{status}", "<span class='status status-success'><a href='javascript: Geo.city.activate("+ city.id +");'>Ativar</a></span>");
            }
            template += cidades;
        });
        Geo.create_datatable(letter, template);
    },

    create_datatable: function(letter, cities)
    {
        var template = $($("#template_table").text());
        template.attr('id', 'table_'+ letter);
        template.find(".cities_content").html(cities);
        template.appendTo(".single_datatable");
        $('#table_'+ letter +' .datatable').dataTable( {
            "bJQueryUI": true,
            "bSortClasses": false,
            "aaSorting": [[0,'asc']],
            "bAutoWidth": true,
            "bInfo": true,
            "sScrollX": "101%",
            "bScrollCollapse": true,
            "sPaginationType": "full_numbers",
            "bRetrieve": true,
            "bDestroy": true,
            "fnInitComplete": function () {
                $('#table_'+ letter +' .dataTables_length > label > select').uniform();
                $('#table_'+ letter +' .dataTables_filter input[type=text]').addClass("text");
                $(".datatable").css("visibility","visible");
            }
        });
    }
}

$(document).ready(function() {
    Geo.init();
});
