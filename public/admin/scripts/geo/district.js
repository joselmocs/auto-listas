
var GeoDistrict = {

    init: function() {
        this.binds();
        this.highlight();
        this.check_terms();
    },

    check_terms: function()
    {
        // Preenchemos a cidade pre-selecionada
        if (_context.estado != null) {
            $("#input_city").removeAttr('disabled');
            if (_context.cidade != null) {
                $("#input_city").select3("data", {id: _context.cidade.id, text: _context.cidade.name});
            }
        }
    },

    binds: function()
    {
        $("#select_estado").select3().on('change', function() {
            $("#input_city").select3('val', '');
            if ($(this).val() == -1) {
                $("#input_city").attr('disabled', 'disabled');
            }
            else {
                $("#input_city").removeAttr('disabled');
            }
        });

        $("#input_city").select3({
            placeholder: "Selecione um município",
            minimumInputLength: 3,
            ajax: {
                url: "/padmin/geo/city/list/jsonp",
                dataType: 'jsonp',
                quietMillis: 500,
                data: function (term, page) {
                    return {
                        q: term,
                        estate_id: $("#select_estado").val()
                    };
                },
                results: function (data, page) {
                    return {
                        results: data.cidades
                    };
                }
            },
            initSelection : function (element, callback) {
                var data = {id: element.val(), text: element.val()};
                callback(data);
            }
        });

        $("#filtrar").click(function() {
            var estado = $("#select_estado").val();
            var cidade = $("#input_city").val() ? ($("#input_city").val()) : -1;
            var bairro = $("#input_district").val() ? ($("#input_district").val()) : -1;
            var status = $("input[name=status_district]:checked").val();

            window.location = '/padmin/geo/district/'+ estado +'/'+ cidade +'/'+ bairro +'/'+ status;
        });

        $("#resetar").click(function() {
            window.location = '/padmin/geo/district';
        });

        $("#add_empresa").click(function() {
            alert("add");
        });
    },

    highlight: function() {
        if ($("#input_district").val()) {
            $('.hightlight_td').highlight($("#input_district").val());
        }
    }
};

$(document).ready(function() {
    GeoDistrict.init();
});