
var BusinessView = {

    init: function() {
        this.binds();
    },

    binds: function()
    {
        $("#editar_empresa").click(function() {
            window.location = '/padmin/business/edit/' + $(this).attr("empresa");
        });
    }
};

$(document).ready(function() {
    BusinessView.init();
});
