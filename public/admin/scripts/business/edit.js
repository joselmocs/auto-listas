
var BusinessEdit = {

    business_id: null,
    estate_id: null,
    container_city: null,

    init: function() {
        this.binds();
        this.parse_edit();
    },

    parse_edit: function()
    {
        if (_context.mode == 'edit') {
            this.estate_id = _context.estado.id;
            this.business_id = _context.empresa.id;

            $("#input_city").select3('data', {
                id: _context.cidade.id,
                text: _context.cidade.name
            }).removeAttr('disabled');

            $("#input_district").select3('data', {
                id: _context.bairro.id,
                text: _context.bairro.name
            }).removeAttr('disabled');
        }
    },

    binds: function()
    {
        $("#input_city").select3({
            placeholder: "Selecione um município",
            minimumInputLength: 3,
            ajax: {
                url: "/padmin/geo/city/list/jsonp",
                dataType: 'jsonp',
                quietMillis: 500,
                data: function (term, page) {
                    return {
                        q: term,
                        estate_id: BusinessEdit.estate_id
                    };
                },
                results: function (data, page) {
                    return {
                        results: data.cidades
                    };
                }
            },
            initSelection : function (element, callback) {
                var data = {id: element.val(), text: element.val()};
                callback(data);
            }
        }).on('change', function() {
            $("#input_district").select3('val', '');
            if ($(this).val() == -1) {
                $("#input_district").attr('disabled', 'disabled');
            }
            else {
                $("#input_district").removeAttr('disabled');
            }
        });

        $("#input_district").select3({
            placeholder: "Selecione um bairro",
            minimumInputLength: 2,
            ajax: {
                url: "/padmin/geo/district/list/jsonp",
                dataType: 'jsonp',
                quietMillis: 500,
                data: function (term, page) {
                    return {
                        q: term,
                        city_id: $("#input_city").val()
                    };
                },
                results: function (data, page) {
                    return {
                        results: data.bairros
                    };
                }
            },
            initSelection : function (element, callback) {
                var data = {id: element.val(), text: element.val()};
                callback(data);
            }
        });

        $("#input_categories").select3();

        $("#select_estate").select3().on('change', function() {
            $("#input_city").select3('val', '');

            if ($(this).val() == -1) {
                $("#input_city").attr('disabled', 'disabled');
            }
            else {
                $("#input_city").removeAttr('disabled');
            }

            $("#input_district").select3('val', '');
            if ($("#input_city").val() == -1 || $("#input_city").val() == "") {
                $("#input_district").attr('disabled', 'disabled');
            }
            else {
                $("#input_district").removeAttr('disabled');
            }

            BusinessEdit.estate_id = $(this).val();
        });

        $("button[name='button_save']").click(function() {
            if (BusinessEdit.validate()) {
                BusinessEdit.add();
            }
        });

        $("button[name='button_back']").click(function() {
            window.location = _context.url_previous;
        });

        $("#select_state").change(function() {
            BusinessEdit.estate_id = $(this).val();
            BusinessEdit.city.bind();
        });
    },

    validate: function() {
        BusinessEdit.error.clear();

        var error = false;
        $('input.required, select.required').each(function() {
            if (!$(this).val() || $(this).val() == -1) {
                BusinessEdit.error.append($(this));
                error = true;
            }
        });

        return !error;
    },

    error:
    {
        append: function(field) {
            var label_error = $('<label class="error label_error">Este campo é obrigatório.</label>');
            label_error.appendTo(field.parent());
        },

        clear: function() {
            $(".label_error").remove();
            $("#error_edit_business").hide();
            $("span[name=msg_error]").html("");
        },

        show: function(errors) {
            $.each(errors, function(x, message) {
                $("span[name=msg_error]").append(message +"<br />");
            });
            $("#error_edit_business").toggle(200);
        }
    },

    after_edit:
    {
        new: function() {
            BusinessEdit.loading.show();
            window.location = '/padmin/business/add';
        },

        view: function() {
            BusinessEdit.loading.show();
            window.location = '/padmin/business/view/'+ BusinessEdit.business_id;
        },

        edit: function() {
            BusinessEdit.loading.show();
            window.location = '/padmin/business/edit/'+ BusinessEdit.business_id;
        }
    },

    add: function() {
        BusinessEdit.loading.show();

        $.ajax({
            type: "POST",
            url: '/padmin/business/save',
            data: {
                'id': BusinessEdit.business_id,
                'mode': _context.mode,
                'name': $("#input_name").val(),
                'address': $("#input_address").val(),
                'zipcode': $("#input_zipcode").val(),
                'district_id': $("#input_district").val(),
                'ddd': $("#input_ddd").val(),
                'phone': $("#input_phone").val(),
                'categories': $("#input_categories").val(),
                'website': $("#input_website").val(),
                'facebook': $("#input_facebook").val(),
                'twitter': $("#input_twitter").val(),
                'active': $("input[name=status_business]:checked").val()
            },
            dataType: 'json',
            error: function() {
                BusinessEdit.loading.hide();
            },
            success: function(response) {
                BusinessEdit.loading.hide();
                if ('success' in response) {
                    BusinessEdit.business_id = response.id;
                    $("#dialog_success").dialog('open');
                }
                else {
                    var errors = [];
                    $.each(response, function(x, field) {
                        $.each(field, function(y, msg) {
                            errors.push(msg);
                        });
                    });

                    if (errors.length) {
                        BusinessEdit.error.show(errors);
                    }
                }
            }
        });
    },

    loading: {
        show: function() {
            $("#loading_overlay, #loading_overlay div").show();
        },
        hide: function() {
            $("#loading_overlay, #loading_overlay div").hide();
        }
    }
};

$(document).ready(function() {
    BusinessEdit.init();
});