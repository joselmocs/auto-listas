
var BusinessSearch = {

    init: function() {
        this.binds();
        this.highlight();
        this.check_terms();
    },

    check_terms: function()
    {
        // Preenchemos a cidade pre-selecionada
        if (_context.estado != null) {
            $("#input_city").removeAttr('disabled');
            if (_context.cidade != null) {
                $("#input_city").select3("data", {id: _context.cidade.id, text: _context.cidade.name});
            }
        }

        // Preenchemos o bairro pre-selecionado
        if (_context.cidade != null) {
            $("#input_district").removeAttr('disabled');
            if (_context.bairro != null) {
                $("#input_district").select3("data", {id: _context.bairro.id, text: _context.bairro.name});
            }
        }
    },

    binds: function()
    {
        $("#select_categoria").select3();

        $("#select_estado").select3().on('change', function() {
            $("#input_city").select3('val', '');
            if ($(this).val() == -1) {
                $("#input_city").attr('disabled', 'disabled');
            }
            else {
                $("#input_city").removeAttr('disabled');
            }
        });

        $("#input_city").select3({
            placeholder: "Selecione um município",
            minimumInputLength: 3,
            ajax: {
                url: "/padmin/geo/city/list/jsonp",
                dataType: 'jsonp',
                quietMillis: 500,
                data: function (term, page) {
                    return {
                        q: term,
                        estate_id: $("#select_estado").val()
                    };
                },
                results: function (data, page) {
                    return {
                        results: data.cidades
                    };
                }
            },
            initSelection : function (element, callback) {
                var data = {id: element.val(), text: element.val()};
                callback(data);
            }
        }).on('change', function() {
            $("#input_district").select3('val', '');
            if ($(this).val() == -1) {
                $("#input_district").attr('disabled', 'disabled');
            }
            else {
                $("#input_district").removeAttr('disabled');
            }
        });

        $("#input_district").select3({
            placeholder: "Selecione um bairro",
            minimumInputLength: 2,
            ajax: {
                url: "/padmin/geo/district/list/jsonp",
                dataType: 'jsonp',
                quietMillis: 500,
                data: function (term, page) {
                    return {
                        q: term,
                        city_id: $("#input_city").val()
                    };
                },
                results: function (data, page) {
                    return {
                        results: data.bairros
                    };
                }
            },
            initSelection : function (element, callback) {
                var data = {id: element.val(), text: element.val()};
                callback(data);
            }
        });

        $("#filtrar").click(function() {
            var estado = $("#select_estado").val();
            var cidade = $("#input_city").val() ? ($("#input_city").val()) : -1;
            var bairro = $("#input_district").val() ? ($("#input_district").val()) : -1;
            var categoria = $("#select_categoria").val();
            var nome = $("#input_nome").val().length ? $("#input_nome").val() : '-1';
            var status = $("input[name=status_business]:checked").val();

            window.location = '/padmin/business/search/'+ estado +'/'+ cidade +'/'+ bairro +'/'+ categoria +'/'+ status +'/'+ nome;
        });

        $("#resetar").click(function() {
            window.location = '/padmin/business';
        });

        $("#add_empresa").click(function() {
            window.location = '/padmin/business/add';
        });
    },

    highlight: function() {
        if ($("#input_nome").val()) {
            $('.hightlight_td').highlight($("#input_nome").val());
        }
    }
};

$(document).ready(function() {
    BusinessSearch.init();
});