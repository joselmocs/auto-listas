

var Category = {

    actual_element: null,
    editing: null,
    new_tag_select: null,

    init: function() {
        this.binds();
        this.new_tag_select = $("#container-select").html();
        this.tag_select.bind();
    },

    tag_select: {
        bind: function() {
            $(".select3").select3();
        },

        reset: function() {
            $(".select3").remove();
            var novo_select = $(Category.new_tag_select);
            novo_select.appendTo($("#container-select"));
        }
    },

    binds: function()
    {
        $("#button_add_category").click(function() {
            Category.insert();
        });

        $("#button_edit_category").click(function() {
            Category.edit();
        });

        $("#button_mode_new").click(function() {
            Category.tag_select.reset();
            Category.tag_select.bind();
            Category.mode.new();
        });
    },

    loading: {
        show: function() {
            $("#loading_overlay, #loading_overlay div").show();
        },
        hide: function() {
            $("#loading_overlay, #loading_overlay div").hide();
        }
    },

    mode:
    {
        new: function()
        {
            $("#error_add_category").hide();
            $("span[name=msg_add_category]").html("");

            if (Category.actual_element != null) {
                Category.actual_element.text("Editar");
                Category.actual_element = null;
            }
            Category.editing = null;

            $("#category_field").val("");
            $("div[name=mode_edit]").hide();
            $("div[name=mode_new]").show();
        },

        edit: function(nome, id, element)
        {
            $("#error_add_category").hide();
            $("span[name=msg_add_category]").html("");

            if (Category.actual_element != null) {
                Category.actual_element.text("Editar");
            }
            Category.actual_element = element;
            Category.actual_element.text("Editando");
            Category.editing = id;

            $("#category_field").val(nome);
            $("div[name=mode_new]").hide();
            $("div[name=mode_edit]").show();

            Category.tag_select.reset();

            var tags = element.parent().find("input[name=data-tag]").val().split(',');
            $.each(tags, function(i, tag) {
                if (tag.length) {
                    $("#tags").find("option[value="+ tag +"]").attr("selected", "selected")
                }
            });
            Category.tag_select.bind();

            $("div[name=mode_edit].section").show();
            $("div[name=mode_edit].section").find('h2').html(nome);
        }
    },

    remove: function()
    {
        if (Category.editing != null) {
            window.location = "/padmin/business/category/remove/"+ Category.editing;
        }
    },

    edit: function()
    {
        $("#error_add_category").hide();
        $("span[name=msg_add_category]").html("");

        if (!$("#category_field").val().length) {
            $("span[name=msg_add_category]").html("Por favor, preencha o nome da categoria.");
            $("#error_add_category").toggle(200);
            return;
        }

        Category.loading.show();

        $.ajax({
            type: "POST",
            url: '/padmin/business/category/edit',
            data: {
                'id': Category.editing,
                'category': $("#category_field").val(),
                'tags': $("#tags").val()
            },
            dataType: 'json',
            error: function() {
                Category.loading.hide();
            },
            success: function(response) {
                if ('success' in response) {
                    window.location = '';
                }
                else {
                    Category.loading.hide();
                    var erro = null;
                    $.each(response, function(x, field) {
                        $.each(field, function(y, msg) {
                            erro = msg;
                        });
                    });

                    if (erro) {
                        $("span[name=msg_add_category]").html(erro);
                        $("#error_add_category").toggle(200);
                    }
                }
            }
        });
    },

    insert: function()
    {
        $("#error_add_category").hide();
        $("span[name=msg_add_category]").html("");

        if (!$("#category_field").val().length) {
            $("span[name=msg_add_category]").html("Por favor, preencha o nome da categoria.");
            $("#error_add_category").toggle(200);
            return;
        }

        Category.loading.show();

        $.ajax({
            type: "POST",
            url: '/padmin/business/category/add',
            data: {
                'category': $("#category_field").val(),
                'tags': $("#tags").val()
            },
            dataType: 'json',
            error: function() {
                Category.loading.hide();
            },
            success: function(response) {
                if ('success' in response) {
                    window.location = '';
                }
                else {
                    Category.loading.hide();
                    var erro = null;
                    $.each(response, function(x, field) {
                        $.each(field, function(y, msg) {
                            erro = msg;
                        });
                    });

                    if (erro) {
                        $("span[name=msg_add_category]").html(erro);
                        $("#error_add_category").toggle(200);
                    }
                }
            }
        });
    }
};

$(document).ready(function() {
    Category.init();
});