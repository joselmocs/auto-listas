<?php
return array(

    'identified_by' => array('username', 'email'),

    'groups' => array(
        'super_admin' => array(10, 'Super Admin'),
        'admin' => array(9, 'Administrador'),
        'moderador' => array(8, 'Moderador'),
        'editor' => array(4, 'Editor'),
        'colaborador' => array(3, 'Colaborador'),
        'empresa' => array(2, 'Empresa'),
        'usuario' => array(1, 'Usuário'),
    ),

    // DB prefix for tables
    'prefix' => ''

);
