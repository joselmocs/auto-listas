<?php

class SearchDistrictSeeder extends Seeder {

    public function run()
    {
        $this->command->info('Populando: bairros');
        $cidades = DB::table('cidades')->select(array('id', 'nome', 'uf'))->orderBy('nome')->get();

        foreach ($cidades as $db_cidade) {
            $this->command->info('        : '. strtolower($db_cidade->nome.'-'.$db_cidade->uf));
            $cidade = DB::table('search_city')
                ->select('search_city.id')
                ->where('search_city.name', '=', $db_cidade->nome)
                ->join('search_estate', 'search_estate.id', '=', 'search_city.estate_id')
                ->where('search_estate.short', '=', $db_cidade->uf)
                ->first();

            $bairros = DB::table('bairros')
                ->select(array('uf', 'cidade', 'nome'))
                ->where('cidade', '=', $db_cidade->id)
                ->orderBy('nome')->get();

            foreach ($bairros as $bairro) {
                $newDistrict = new SearchDistrict;
                $newDistrict->name = $bairro->nome;
                $newDistrict->city_id = $cidade->id;
                $newDistrict->save();
            }
        }
    }
}