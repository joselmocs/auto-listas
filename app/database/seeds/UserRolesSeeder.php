<?php

class UserRolesSeeder extends Seeder {

    public function run()
    {
        $this->command->info('Populando: grupos de usuario');

        $prefix = Config::get('verify::prefix');
        // Cria os grupos de usuários
        foreach (Config::get('verify::groups') as $key => $value) {
            DB::table($prefix.'roles')->insertGetId(array(
                'name' => $value[1],
                'slug' => $key,
                'level' => $value[0],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ));
        }
    }
}