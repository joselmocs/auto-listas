<?php

class SearchCityEstateSeeder extends Seeder {

    public function run()
    {
        $this->command->info('Populando: estados e cidades');

        $arquivo = __DIR__ ."\\files\\estados-cidades.json";

        $f = fopen($arquivo, 'r');
        $conteudo = fread($f, filesize($arquivo));
        fclose($f);

        $decoded = json_decode($conteudo, true);

        foreach ($decoded['estados'] as $estado) {
            $this->command->info('        : '. strtolower($estado['nome']));
            $newEstate = new SearchEstate;
            $newEstate->name = $estado['nome'];
            $newEstate->short = $estado['sigla'];
            $newEstate->active = false;
            $newEstate->save();

            foreach ($estado['cidades'] as $cidade) {
                $newCity = new SearchCity;
                $newCity->name = $cidade;
                $newCity->estate_id = $newEstate->id;
                $newCity->save();
            }
        }
    }
}