<?php

class UserTableSeeder extends Seeder {

    public function run()
    {
        $this->command->info('Populando: super-administrador');

        $prefix = Config::get('verify::prefix', '');

        $user_id = DB::table($prefix.'users')->insertGetId(array(
            'username' => 'admin',
            'password' => '$2a$08$rqN6idpy0FwezH72fQcdqunbJp7GJVm8j94atsTOqCeuNvc3PzH3m',
            'salt' => 'a227383075861e775d0af6281ea05a49',
            'email' => 'admin@example.com',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'verified' => 1,
            'disabled' => 0
        ));

        DB::table($prefix.'role_user')->insert(array(
            'role_id' => DB::table($prefix.'roles')->where('slug', '=', 'super_admin')->first()->id,
            'user_id' => $user_id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ));

        $user = User::find($user_id);
        $user->username = "JOSELMO";
        $user->password = "admin";
        $user->email = strtoupper("joselmo@outlook.com");
        $user->save();

        $user->profile->firstname = "Joselmo";
        $user->profile->lastname = "Cardozo";
        $user->profile->save();
    }
}