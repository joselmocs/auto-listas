<?php

class SearchTagSeeder extends Seeder {

    public function run()
    {
        $this->command->info('Populando: etiquetas de pesquisa');

        $new = new SearchTag;
        $new->name = "Auto Peças";
        $new->slug = "auto-pecas";
        $new->save();

        $new = new SearchTag;
        $new->name = "Motores";
        $new->slug = "motores";
        $new->save();
    }
}