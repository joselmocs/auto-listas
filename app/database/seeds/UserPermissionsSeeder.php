<?php

use \Toddish\Verify\Models\Role;
use \Toddish\Verify\Models\Permission;

class UserPermissionsSeeder extends Seeder {

    public $permissions = array(
        // User
        array('USUARIO|Administrador', 'user.admin', ''),
        array('USUARIO|Adicionar', 'user.add', ''),
        array('USUARIO|Editar', 'user.edit', ''),
        array('USUARIO|Remover', 'user.del', ''),
        // SearchCategory
        array('CATEGORIA|Adicionar', 'category.add', ''),
        array('CATEGORIA|Editar', 'category.edit', ''),
        array('CATEGORIA|Remover', 'category.del', ''),
        // SearchTag
        array('ETIQUETA|Adicionar', 'tag.add', ''),
        array('ETIQUETA|Editar', 'tag.edit', ''),
        array('ETIQUETA|Remover', 'tag.del', ''),
        // SearchEstate
        array('ESTADO|Ativar/desativar', 'estate.status', ''),
        // SearchCity
        array('CIDADE|Ativar/desativar', 'city.status', ''),
        // SearchDistrict
        array('BAIRRO|Ativar/desativar', 'district.status', '')
    );

    public function run()
    {
        $this->command->info('Populando: permissoes de grupo de usuario');

        $prefix = Config::get('verify::prefix');

        foreach ($this->permissions as $permission) {
            DB::table($prefix .'permissions')->insert(array(
                'name' => $permission[0],
                'slug' => $permission[1],
                'description' => $permission[2],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ));
        }

        // Adicionamos as permissões para ADMINISTRADOR
        $adminRole = Role::where('slug', '=', 'admin')->firstOrFail();
        foreach ($this->permissions as $permission) {
            $perm = Permission::where('slug', '=', $permission[1])->firstOrFail();
            $adminRole->permissions()->sync(array($perm->id));
        };
    }
}