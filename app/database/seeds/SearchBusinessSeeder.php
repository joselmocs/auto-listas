<?php

class SearchBusinessSeeder extends Seeder {

    public function run()
    {
        $this->command->info('Populando: empresas');

        $new = new SearchBusiness;
        $new->name = "Exata Centro Automotivo";
        $new->slug = "exata-centro-automotivo";
        $new->address = "Rua Lamenha Lins, 1716";
        $new->district_id = SearchDistrict::where('slug', '=', 'centro-curitiba-pr')->firstOrFail()->id;
        $new->zipcode = "30000000";
        $new->phone = "4134343434";
        $new->thumb = "http://i1.ypcdn.com/blob/811b39499aa6ebe9b60d591eda661d7807301a32_70x70_crop.jpg?7b6b651";
        $new->website = "http://www.google.com.br";
        $new->facebook = "centroautomotivo";
        $new->twitter = "twitter";
        $new->save();

        $cat = new SearchBusinessCategory();
        $cat->business_id = $new->id;
        $cat->category_id = SearchCategory::where('slug', '=', 'acessorios')->firstOrFail()->id;
        $cat->save();

        $cat = new SearchBusinessCategory();
        $cat->business_id = $new->id;
        $cat->category_id = SearchCategory::where('slug', '=', 'baterias')->firstOrFail()->id;
        $cat->save();

        $new = new SearchBusiness;
        $new->name = "Auto Peças Ford";
        $new->slug = "auto-pecas-ford";
        $new->address = "R Prof Joao Soares Barcelos, 2085";
        $new->district_id = SearchDistrict::where('slug', '=', 'centro-belo-horizonte-mg')->firstOrFail()->id;
        $new->zipcode = "30000000";
        $new->phone = "4134343434";
        $new->twitter = "twitter";
        $new->save();

        $cat = new SearchBusinessCategory();
        $cat->business_id = $new->id;
        $cat->category_id = SearchCategory::where('slug', '=', 'acessorios')->firstOrFail()->id;
        $cat->save();

        $new = new SearchBusiness;
        $new->name = "Exata Centro Automotivo";
        $new->slug = "exata-centro-automotivo";
        $new->address = "Rua Lamenha Lins, 1716";
        $new->district_id = SearchDistrict::where('slug', '=', 'centro-curitiba-pr')->firstOrFail()->id;
        $new->zipcode = "30000000";
        $new->phone = "3134343434";
        $new->website = "http://www.google.com.br";
        $new->facebook = "centroautomotivo";
        $new->save();

        $cat = new SearchBusinessCategory();
        $cat->business_id = $new->id;
        $cat->category_id = SearchCategory::where('slug', '=', 'baterias')->firstOrFail()->id;
        $cat->save();

        $cat = new SearchBusinessCategory();
        $cat->business_id = $new->id;
        $cat->category_id = SearchCategory::where('slug', '=', 'suspencao')->firstOrFail()->id;
        $cat->save();

        $new = new SearchBusiness;
        $new->name = "Auto Peças Ford";
        $new->slug = "auto-pecas-ford";
        $new->address = "R Prof Joao Soares Barcelos, 2085";
        $new->district_id = SearchDistrict::where('slug', '=', 'centro-belo-horizonte-mg')->firstOrFail()->id;
        $new->zipcode = "30000000";
        $new->phone = "3134343434";
        $new->save();

        $cat = new SearchBusinessCategory();
        $cat->business_id = $new->id;
        $cat->category_id = SearchCategory::where('slug', '=', 'baterias')->firstOrFail()->id;
        $cat->save();

        $cat = new SearchBusinessCategory();
        $cat->business_id = $new->id;
        $cat->category_id = SearchCategory::where('slug', '=', 'suspencao')->firstOrFail()->id;
        $cat->save();

        $cat = new SearchBusinessCategory();
        $cat->business_id = $new->id;
        $cat->category_id = SearchCategory::where('slug', '=', 'acessorios')->firstOrFail()->id;
        $cat->save();
    }
}