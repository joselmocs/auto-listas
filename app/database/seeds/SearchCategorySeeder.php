<?php

class SearchCategorySeeder extends Seeder {

    public function run()
    {
        $this->command->info('Populando: categorias de pesquisa');

        $new = new SearchCategory;
        $new->name = "Acessórios";
        $new->slug = "acessorios";
        $new->save();

        $new = new SearchCategory;
        $new->name = "Baterias";
        $new->slug = "baterias";
        $new->save();

        $new = new SearchCategory;
        $new->name = "Suspenção";
        $new->slug = "suspencao";
        $new->save();
    }
}