<?php

use Illuminate\Database\Migrations\Migration;

class CreateSearchCategory extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        // Create the category table
        Schema::create('search_category', function($table)
        {
            $table->increments('id');
            $table->string('name', 128)->nullable();
            $table->string('slug', 128)->index();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('search_category');
	}

}
