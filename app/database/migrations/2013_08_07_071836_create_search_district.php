<?php

use Illuminate\Database\Migrations\Migration;

class CreateSearchDistrict extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        // Create the category table
        Schema::create('search_district', function($table)
        {
            $table->increments('id');
            $table->string('name', 128)->nullable();
            $table->string('slug', 128)->index();
            $table->string('search_tag', 128)->index();
            $table->integer('city_id')->unsigned()->nullable();
            $table->boolean('active')->default(true);
            $table->timestamps();

            $table->foreign('city_id')->references('id')->on('search_city');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('search_district');
	}

}