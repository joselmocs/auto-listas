<?php

use Illuminate\Database\Migrations\Migration;

class CreateSearchBusiness extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        // Create the business table
        Schema::create('search_business', function($table)
        {
            $table->increments('id');
            $table->string('name', 128)->nullable();
            $table->string('slug', 128)->index();

            $table->string('address', 256)->nullable();
            $table->integer('district_id')->unsigned()->nullable();

            $table->string('zipcode', 8)->nullable();

            $table->string('phone', 12)->nullable();
            $table->string('thumb', 256)->nullable();

            $table->string('website', 128)->nullable();
            $table->string('facebook', 128)->nullable();
            $table->string('twitter', 128)->nullable();

            $table->integer('rating_votes')->default(0);
            $table->integer('rating_points')->default(0);

            $table->integer('relevance')->default(0);
            $table->boolean('active')->default(true);

            $table->timestamps();

            $table->foreign('district_id')->references('id')->on('search_district');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('search_business');
	}

}