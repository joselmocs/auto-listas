<?php

use Illuminate\Database\Migrations\Migration;

class CreateSearchCity extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        // Create the category table
        Schema::create('search_city', function($table)
        {
            $table->increments('id');
            $table->string('name', 128)->nullable();
            $table->string('slug', 128)->index();
            $table->string('search_tag', 128)->index();
            $table->integer('estate_id')->unsigned()->nullable();
            $table->integer('relevance')->default(0);
            $table->boolean('active')->default(true);
            $table->timestamps();

            $table->foreign('estate_id')->references('id')->on('search_estate');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('search_city');
	}

}