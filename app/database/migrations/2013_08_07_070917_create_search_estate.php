<?php

use Illuminate\Database\Migrations\Migration;

class CreateSearchEstate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        // Create the category table
        Schema::create('search_estate', function($table)
        {
            $table->increments('id');
            $table->string('name', 32)->index();
            $table->string('slug', 32)->index();
            $table->string('short', 2);
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('search_estate');
	}

}