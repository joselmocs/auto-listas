<?php

use Illuminate\Database\Migrations\Migration;

class CreateSearchBusinessCategory extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        // Create the category table
        Schema::create('search_business_category', function($table)
        {
            $table->integer('business_id')->unsigned()->nullable();
            $table->integer('category_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('business_id')->references('id')->on('search_business');
            $table->foreign('category_id')->references('id')->on('search_category');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('search_business_category');
	}

}