<?php

use Illuminate\Database\Migrations\Migration;

class CreateSearchTag extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        // Create the tags table
        Schema::create('search_tag', function($table)
        {
            $table->increments('id');
            $table->string('name', 64)->nullable();
            $table->string('slug', 64)->index();
            $table->integer('relevance')->default(0);
            $table->timestamps();
        });

        Schema::create('search_tag_category', function($table)
        {
            $table->integer('tag_id')->unsigned()->nullable();
            $table->integer('category_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('tag_id')->references('id')->on('search_tag');
            $table->foreign('category_id')->references('id')->on('search_category');
        });

        Schema::create('search_tag_business', function($table)
        {
            $table->integer('tag_id')->unsigned()->nullable();
            $table->integer('business_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('tag_id')->references('id')->on('search_tag');
            $table->foreign('business_id')->references('id')->on('search_business');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('search_tag_category');
        Schema::dropIfExists('search_tag_business');
        Schema::dropIfExists('search_tag');
	}

}