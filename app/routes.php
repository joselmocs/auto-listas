<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Site Routes
|--------------------------------------------------------------------------
*/

Route::any('/', 'SiteController@any_index');

Route::get('/proxy/search/terms', 'SearchController@json_search_term');
Route::get('/proxy/search/location', 'SearchController@json_search_location');

Route::any('/curitiba-pr/auto-repair-service', 'SiteController@get_search');


/*
|--------------------------------------------------------------------------
| Backend Routes
|--------------------------------------------------------------------------
*/

Route::get('/padmin', 'BackendController@get_index');

Route::group(array('prefix' => 'padmin'), function() {
    // Logout
    Route::get('logout', 'BackendController@get_logout');

    Route::group(array('before' => 'csrf'), function() {
        // Login
        Route::post('login', 'BackendController@post_login');
    });

    Route::group(array('before' => 'auth'), function()
    {
        /*
        |--------------------------------------------------------------------------
        | Business Routes
        |--------------------------------------------------------------------------
        */
        Route::get('business', 'BackendBusinessController@get_search_index');
        Route::get('business/search/{estado}/{cidade}/{bairro}/{categoria}/{status}/{nome}', 'BackendBusinessController@get_search_filtered');
        Route::get('business/add', 'BackendBusinessController@get_add');
        Route::post('business/save', 'BackendBusinessController@post_json_save');
        Route::get('business/edit/{empresa}', 'BackendBusinessController@get_edit');
        Route::post('business/edit/{empresa}', 'BackendBusinessController@post_edit');
        Route::get('business/view/{empresa}', 'BackendBusinessController@get_view');

        /*
        |--------------------------------------------------------------------------
        | Category Routes
        |--------------------------------------------------------------------------
        */
        Route::any('business/category', 'BackendCategoryController@any_index');
        Route::post('business/category/add', 'BackendCategoryController@post_json_add');
        Route::post('business/category/edit', 'BackendCategoryController@post_json_edit');
        Route::get('business/category/remove/{category}', 'BackendCategoryController@get_remove');

        /*
        |--------------------------------------------------------------------------
        | Tag Routes
        |--------------------------------------------------------------------------
        */
        Route::any('business/tag', 'BackendTagController@any_index');
        Route::post('business/tag/add', 'BackendTagController@post_json_add');
        Route::post('business/tag/edit', 'BackendTagController@post_json_edit');
        Route::get('business/tag/remove/{tag}', 'BackendTagController@get_remove');

        /*
        |--------------------------------------------------------------------------
        | Geo Routes
        |--------------------------------------------------------------------------
        */
        Route::get('geo/estate', 'BackendGeoController@get_list_estate');
        Route::get('geo/estate/activate/{estado}', 'BackendGeoController@get_estate_activate');
        Route::get('geo/estate/deactivate/{estado}', 'BackendGeoController@get_estate_deactivate');

        Route::get('geo/city/list/jsonp', 'BackendGeoController@jsonp_list_city');
        Route::get('geo/city/{estado?}', 'BackendGeoController@get_list_city');
        Route::post('geo/city/{estado}', 'BackendGeoController@post_json_list_city');
        Route::post('geo/city/active/{city}', 'BackendGeoController@post_active_city');
        Route::post('geo/city/deactive/{city}', 'BackendGeoController@post_deactive_city');


        Route::get('geo/district/list/jsonp', 'BackendGeoController@jsonp_list_district');
        Route::get('geo/district', 'BackendGeoController@get_search_district');
        Route::get('geo/district/{estado}/{cidade}/{bairro}/{status}', 'BackendGeoController@get_search_district_filtered');
        Route::get('geo/district/activate/{bairro}', 'BackendGeoController@get_district_activate');
        Route::get('geo/district/deactivate/{bairro}', 'BackendGeoController@get_district_deactivate');

        /*
        |--------------------------------------------------------------------------
        | User Routes
        |--------------------------------------------------------------------------
        */
        Route::any('user/system', 'BackendUserController@get_users_system');

    });
});


