@extends('admin/template')

@section('scripts')
{{ HTML::script('/admin/scripts/business/view.js') }}
@endsection

@section('styles')
{{ HTML::style('/admin/styles/custom.css') }}
@endsection

@section('conteudo')
<div>
    <div id="wrapper">

        @include('admin/include_user_box')

        <div id="main_container" class="main_container container_16 clearfix">

            @include('admin/include_menu')

            <div class="flat_area grid_8">
                <h2>{{ $context->empresa->name }}</h2>
                <p>
                    {{ $context->empresa->address  }}<br />
                    {{ $context->empresa->zipcode  }} - {{ $context->empresa->city->name  }} - {{ $context->empresa->city->estate->short  }}<br />
                    Tel.: {{ $context->empresa->phone }}
                </p>
            </div>

            <div class="flat_area grid_8">
                <button class="blue send_right text_only has_text dialog_button img_icon" id="editar_empresa" empresa="{{ $context->empresa->id  }}">
                    <img src="/admin/images/icons/small/white/marker.png">
                    <span>Editar empresa</span>
                </button>
            </div>

            <div class="box grid_8">
                <h2 class="box_head">Resumo das Estatísticas</h2>
                <div class="toggle_container">
                    <div class="block">
                        <ul class="flat">
                            <li><span class="spark_line small random_number_5 spark_inline"></span><strong>0 </strong>Exibições na pesquisa</li>
                            <li><span class="spark_line small random_number_5 spark_inline"></span><strong>0 </strong>Visualizações</li>
                            <li><span class="spark_line small random_number_5 spark_inline"></span><strong>0 </strong>Relevância</li>
                            <li><span class="spark_line small random_number_5 spark_inline"></span><strong>0 </strong>Análises</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="box grid_8">
                <h2 class="box_head">Categorias</h2>
                <div class="toggle_container">
                    <div class="block">
                        <ul class="flat">
                        @if (count($context->empresa->categories))
                            @foreach ($context->empresa->categories as $categoria)
                            <li>{{ $categoria->name }}</li>
                            @endforeach
                        @else
                            <li>Nenhuma categoria definida para esta empresa.</li>
                        @endif
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection