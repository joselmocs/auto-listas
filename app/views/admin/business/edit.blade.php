@extends('admin/template')

@section('scripts')
{{ HTML::script('/admin/scripts/business/edit.js') }}
@endsection

@section('styles')
{{ HTML::style('/admin/styles/custom.css') }}
@endsection

@section('conteudo')
<div>
    <div id="wrapper">

        @include('admin/include_user_box')

        <div id="main_container" class="main_container container_16 clearfix">

            @include('admin/include_menu')

            <div class="flat_area grid_16">
                @if ($context->mode == 'new')
                <h2>Cadastro de Empresa</h2>
                @else
                <h2>{{ $context->empresa->name }}</h2>
                @endif
                <button class="send_right dark img_icon has_text" name="button_back">
                    <img src="/admin/images/icons/small/white/bended_arrow_right.png">
                    <span>CANCELAR / VOLTAR</span>
                </button>
            </div>


            <div id="error_edit_business" style="padding: 0px 10px;display: none;">
                <div class="alert alert_red">
                    <img height="24" width="24" src="/admin/images/icons/small/white/alert_2.png">
                    <span name="msg_error"></span>
                </div>
            </div>

            <div class="box grid_16">
                <h2 class="box_head"></h2>
                <h2 class="section">Informações da Empresa</h2>

                <div class="columns clearfix">
                    <fieldset class="label_side top">
                        <label>Nome</label>
                        <div class="clearfix">
                            <input type="text" class="required text" id="input_name" value="@if ($context->mode == 'edit'){{ $context->empresa->name }}@endif">
                            <div class="required_tag hover left"></div>
                        </div>
                    </fieldset>
                </div>

                <div class="columns clearfix">
                    <fieldset class="label_side top">
                        <label for="text_field_inline">Endereço</label>
                        <div class="clearfix">
                            <input type="text" class="text" id="input_address" value="@if ($context->mode == 'edit'){{ $context->empresa->address }}@endif">
                        </div>
                    </fieldset>
                </div>

                <div class="columns clearfix">
                    <div class="col_50">
                        <fieldset class="label_top top">
                            <label for="text_field_inline">Estado</label>
                            <div>
                                <select class="select3 required" id="select_estate" style="width: 100%;">
                                    <optgroup label="Selecione uma opção">
                                        <option value="-1">Selecione um estado</option>
                                    </optgroup>
                                    <optgroup label="Lista de estados">
                                        @foreach ($context->estados as $estado)
                                        <option value="{{ $estado->id }}" @if ($context->mode == 'edit' && $context->empresa->estate()->id == $estado->id)selected="selected"@endif>{{ $estado->name }}</option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                            <div class="required_tag hover left"></div>
                        </fieldset>
                    </div>
                    <div class="col_50">
                        <fieldset class="label_top top right">
                            <label for="text_field_inline">Município</label>
                            <div id="container-city">
                                <input class="select3 required" style="width: 100%;" id="input_city" disabled="disabled">
                                <div class="required_tag hover left"></div>
                            </div>
                        </fieldset>
                    </div>
                </div>

                <div class="columns clearfix">
                    <div class="col_50">
                        <fieldset class="label_top top">
                            <label for="text_field_inline">Bairro</label>
                            <div id="container-city">
                                <input class="select3 required" style="width: 100%;" id="input_district" disabled="disabled">
                                <div class="required_tag hover left"></div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col_50">
                        <fieldset class="label_top top">
                            <label for="text_field_inline">CEP<span>00000-000</span></label>
                            <div class="clearfix">
                                <input type="text" class="text" id="input_zipcode" value="@if ($context->mode == 'edit'){{ $context->empresa->zipcode }}@endif">
                            </div>
                        </fieldset>
                    </div>
                </div>

                <div class="columns clearfix">
                    <div class="col_50">
                        <fieldset class="label_side label_small bottom">
                            <label for="text_field_inline">DDD</label>
                            <div class="clearfix">
                                <input type="text" class="text" id="input_ddd" value="">
                            </div>
                        </fieldset>
                    </div>
                    <div class="col_50">
                        <fieldset class="label_side label_small bottom">
                            <label for="text_field_inline">Telefone</label>
                            <div class="clearfix">
                                <input type="text" class="required text" id="input_phone" value="@if ($context->mode == 'edit'){{ $context->empresa->phone }}@endif">
                                <div class="required_tag hover left"></div>
                            </div>
                        </fieldset>
                    </div>
                </div>

            </div>


            <div class="box grid_16">
                <h2 class="box_head"></h2>
                <h2 class="section">Informações Adicionais</h2>

                <div class="columns clearfix">
                    <fieldset class="label_side top">
                        <label for="text_field_inline">Categorias</label>
                        <div id="container-select">
                            <select class="select3" style="width: 100%;" id="input_categories" multiple="multiple">
                                @foreach($context->categorias as $categoria)
                                <option value="{{ $categoria->id }}" @if ($context->mode == 'edit' && in_array($categoria->id, $context->ids_categorias))selected="selected"@endif>{{ $categoria->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </fieldset>
                </div>

                <div class="columns clearfix">
                    <fieldset class="label_side top">
                        <label>Website</label>
                        <div class="clearfix">
                            <input type="text" class="text" id="input_website" value="@if ($context->mode == 'edit'){{ $context->empresa->website }}@endif">
                        </div>
                    </fieldset>
                </div>

                <div class="columns clearfix">
                    <div class="col_50">
                        <fieldset class="label_side label_small bottom">
                            <label for="text_field_inline">Facebook</label>
                            <div class="clearfix">
                                <input type="text" class="text" id="input_facebook" value="@if ($context->mode == 'edit'){{ $context->empresa->facebook }}@endif">
                            </div>
                        </fieldset>
                    </div>
                    <div class="col_50">
                        <fieldset class="label_side label_small bottom">
                            <label for="text_field_inline">Twitter</label>
                            <div class="clearfix">
                                <input type="text" class="text" id="input_twitter" value="@if ($context->mode == 'edit'){{ $context->empresa->twitter }}@endif">
                            </div>
                        </fieldset>
                    </div>
                </div>

                <div class="columns clearfix">
                    <div>
                        <fieldset class="label_side top">
                            <label>Status</label>
                            <div class="clearfix">
                                <label for="ativo"><input type="radio" name="status_business" id="ativo" value="1" @if (($context->mode == 'edit' && $context->empresa->active) || $context->mode == 'new')checked="checked"@endif>Ativo</label>
                                <label for="inativo"><input type="radio" name="status_business" id="inativo" value="0" @if ($context->mode == 'edit' && !$context->empresa->active)checked="checked"@endif>Inativo</label>
                            </div>
                        </fieldset>
                    </div>
                </div>

                <div class="button_bar clearfix">
                    <button class="green dark img_icon has_text" name="button_save">
                        <img src="/admin/images/icons/small/white/bended_arrow_right.png">
                        <span>SALVAR</span>
                    </button>
                    <button class="send_right dark img_icon has_text" name="button_back">
                        <img src="/admin/images/icons/small/white/bended_arrow_right.png">
                        <span>CANCELAR / VOLTAR</span>
                    </button>
                </div>

            </div>

        </div>
    </div>
</div>

<div id="loading_overlay">
    <div class="loading_message round_bottom">
        <img src="/admin/images/interface/loading.gif" alt="loading" />
    </div>
</div>

<div id="dialog_success" class="dialog_content narrow no_dialog_titlebar" title="">
    <div class="block">
        <div class="section">
            @if ($context->mode == "new")
            <h2>Empresa cadastrada com sucesso!</h2>
            @endif
            @if ($context->mode == "edit")
            <h2>Empresa editada com sucesso!</h2>
            @endif
            <div class="dashed_line"></div>
            O que deseja fazer?
        </div>
        <div class="button_bar clearfix">
            <button class="dark green no_margin_bottom close_dialog" onclick="BusinessEdit.after_edit.view();">
                <div class="ui-icon ui-icon-check"></div>
                <span>Visualizar Empresa</span>
            </button>
            @if ($context->mode == "new")
            <button class="light send_right close_dialog" onclick="BusinessEdit.after_edit.add();">
                <div class="ui-icon ui-icon-check"></div>
                <span>Adicionar uma empresa</span>
            </button>
            @endif
            @if ($context->mode == "edit")
            <button class="light send_right close_dialog" onclick="BusinessEdit.after_edit.edit();">
                <div class="ui-icon ui-icon-check"></div>
                <span>Continuar editando</span>
            </button>
            @endif
        </div>
    </div>
</div>

@endsection