
<div class="box grid_16">
    <h2 class="box_head">Filtros</h2>
    <div class="controls">
        <a href="#" class="toggle"></a>
    </div>
    <div class="toggle_container">
        <div class="block">

            <div class="columns clearfix">
                <div class="col_50">
                    <fieldset class="label_top top">
                        <label for="text_field_inline">Estado</label>
                        <div>
                            <select class="select3" style="width: 100%;" id="select_estado">
                                <optgroup label="Selecione uma opção">
                                    <option value="-1">Todos os estados</option>
                                </optgroup>
                                <optgroup label="Lista de estados">
                                @foreach ($context->estados as $estado)
                                    @if ($context->filtros && $context->filtros['estado'] == $estado->id)
                                    <option value="{{ $estado->id }}" selected="selected">{{ $estado->name }}</option>
                                    @else
                                    <option value="{{ $estado->id }}">{{ $estado->name }}</option>
                                    @endif
                                @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </fieldset>
                </div>
                <div class="col_50">
                    <fieldset class="label_top top right">
                        <label for="text_field_inline">Município</label>
                        <div>
                            <input class="select3" style="width: 100%;" id="input_city" disabled="disabled">
                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="columns clearfix">
                <div class="col_50">
                    <fieldset class="label_top top">
                        <label for="text_field_inline">Bairro</label>
                        <div>
                            <input class="select3" style="width: 100%;" id="input_district" disabled="disabled">
                        </div>
                    </fieldset>
                </div>
                <div class="col_50">
                    <fieldset class="label_top top right">
                        <label for="text_field_inline">Categoria</label>
                        <div>
                            <select class="select3" style="width: 100%;" id="select_categoria">
                                <optgroup label="Selecione uma opção">
                                    <option value="-1">Todas as categorias</option>
                                    <option value="0" @if ($context->filtros && $context->filtros['categoria'] == 0)selected="selected"@endif>Empresas sem categoria</option>
                                </optgroup>
                                <optgroup label="Lista de Categorias">
                                    @foreach ($context->categorias as $category)
                                    @if ($context->filtros && $context->filtros['categoria'] == $category->id)
                                    <option value="{{ $category->id }}" selected="selected">{{ $category->name }}</option>
                                    @else
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endif
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="columns clearfix">
                <div class="col_50">
                    <fieldset class="label_top top bottom">
                        <label for="text_field_inline">Empresa</label>
                        <div>
                            <input type="text" class="text" id="input_nome" value="@if ($context->filtros){{ $context->filtros['nome'] }}@endif">
                        </div>
                    </fieldset>
                </div>
                <div class="col_50">
                    <fieldset class="label_top top bottom right">
                        <label for="text_field_inline">Status</label>
                        <div class="clearfix">
                            <div style="height: 32px;">
                                <label for="todos" class="send_left" style="padding-right: 20px;">
                                    <input type="radio" name="status_business" id="todos" value="-1" @if (!$context->filtros || $context->filtros['status'] == -1)checked="checked"@endif>
                                    Todos
                                </label>
                                <label for="ativo" class="send_left" style="padding-right: 20px;">
                                    <input type="radio" name="status_business" id="ativo" value="1" @if ($context->filtros && $context->filtros['status'] == 1)checked="checked"@endif>
                                    Ativos
                                </label>
                                <label for="inativo" class="send_left" style="padding-right: 20px;">
                                    <input type="radio" name="status_business" id="inativo" value="0" @if ($context->filtros && $context->filtros['status'] == 0)checked="checked"@endif>
                                    Inativos
                                </label>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="button_bar clearfix">
                <button class="green dark img_icon has_text" id="filtrar">
                    <img src="/admin/images/icons/small/white/bended_arrow_right.png">
                    <span>Filtrar</span>
                </button>

                <button class="grey dark send_right img_icon has_text" id="resetar">
                    <img src="/admin/images/icons/small/white/bended_arrow_left.png">
                    <span>resetar filtros</span>
                </button>
            </div>
        </div>
    </div>
</div>