@extends('admin/template')

@section('scripts')
{{ HTML::script('/scripts/plugins/jquery.highlight-4.js') }}
{{ HTML::script('/admin/scripts/business/search.js') }}
@endsection

@section('styles')
{{ HTML::style('/admin/styles/custom.css') }}
@endsection

@section('conteudo')
<div>
    <div id="wrapper">

        @include('admin/include_user_box')

        <div id="main_container" class="main_container container_16 clearfix">

            @include('admin/include_menu')

            <div class="flat_area grid_16">
                <h2>Lista de Empresas</h2>
                <button class="green send_right text_only has_text dialog_button img_icon" id="add_empresa">
                    <img src="/admin/images/icons/small/white/marker.png">
                    <span>Adicionar empresa</span>
                </button>
            </div>

            @include('admin/business/include_filters')

            @if ($context->filtros != null)
            <div class="box grid_16 single_datatable">
                <h2 class="box_head">Resultado</h2>
                <div class="block">
                    <table class="static">
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Telefone</th>
                            <th>Localização</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @if (count($context->empresas) > 0)
                            @foreach($context->empresas as $empresa)
                            <tr>
                                <td class="hightlight_td">{{ $empresa->name }}</td>
                                <td>{{ $empresa->phone }}</td>
                                <td>{{ $empresa->district->name }},
                                    {{ $empresa->district->city->name }} -
                                    {{ $empresa->district->city->estate->short }}</td>
                                <td style="text-align: right;">
                                    <span class="status status-success"><a href="/padmin/business/view/{{ $empresa->id }}">detalhes</a></span>
                                    <span class="status status-warning"><a href="/padmin/business/edit/{{ $empresa->id }}">editar</a></span>
                                </td>
                            </tr>
                            @endforeach
                        @else
                        <tr>
                            <td colspan="4">Nenhuma empresa encontrada</td>
                        </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            {{ $context->empresas->links() }}
            @endif

        </div>

        <div id="loading_overlay">
            <div class="loading_message round_bottom">
                <img src="/admin/images/interface/loading.gif" alt="loading" />
            </div>
        </div>
    </div>
</div>
@endsection