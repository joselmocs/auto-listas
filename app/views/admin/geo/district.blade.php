@extends('admin/template')

@section('scripts')
{{ HTML::script('/scripts/plugins/jquery.highlight-4.js') }}
{{ HTML::script('/admin/scripts/geo/district.js') }}
@endsection

@section('styles')
{{ HTML::style('/admin/styles/custom.css') }}
@endsection

@section('conteudo')
<div>
    <div id="wrapper">

        @include('admin/include_user_box')

        <div id="main_container" class="main_container container_16 clearfix">

            @include('admin/include_menu')

            <div class="flat_area grid_16">
                <h2>Lista de Bairros</h2>
                <p>Empresas cadastradas em estados desativados <strong>não</strong> irão aparecer nos resultados de busca no site.</p>
            </div>

            <div class="box grid_16">
                <h2 class="box_head">Filtros</h2>
                <div class="controls">
                    <a href="#" class="toggle"></a>
                </div>
                <div class="toggle_container">
                    <div class="block">

                        <div class="columns clearfix">
                            <div class="col_50">
                                <fieldset class="label_top top">
                                    <label for="text_field_inline">Estado</label>
                                    <div>
                                        <select class="select3" style="width: 100%;" id="select_estado">
                                            <optgroup label="Selecione uma opção">
                                                <option value="-1">Todos os estados</option>
                                            </optgroup>
                                            <optgroup label="Lista de estados">
                                                @foreach ($context->estados as $estado)
                                                @if ($context->filtros && $context->filtros['estado'] == $estado->id)
                                                <option value="{{ $estado->id }}" selected="selected">{{ $estado->name }}</option>
                                                @else
                                                <option value="{{ $estado->id }}">{{ $estado->name }}</option>
                                                @endif
                                                @endforeach
                                            </optgroup>
                                        </select>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col_50">
                                <fieldset class="label_top top right">
                                    <label for="text_field_inline">Município</label>
                                    <div>
                                        <input class="select3" style="width: 100%;" id="input_city" disabled="disabled">
                                    </div>
                                </fieldset>
                            </div>
                        </div>

                        <div class="columns clearfix">
                            <div class="col_50">
                                <fieldset class="label_top bottom">
                                    <label for="text_field_inline">Bairro</label>
                                    <div>
                                        <input type="text" class="text" id="input_district" value="@if ($context->filtros){{ $context->filtros['bairro'] }}@endif">
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col_50">
                                <fieldset class="label_top bottom right">
                                    <label for="text_field_inline">Status</label>
                                    <div class="clearfix">
                                        <div style="height: 32px;">
                                            <label for="todos" class="send_left" style="padding-right: 20px;">
                                                <input type="radio" name="status_district" id="todos" value="-1" @if (!$context->filtros || $context->filtros['status'] == -1)checked="checked"@endif>
                                                Todos
                                            </label>
                                            <label for="ativo" class="send_left" style="padding-right: 20px;">
                                                <input type="radio" name="status_district" id="ativo" value="1" @if ($context->filtros && $context->filtros['status'] == 1)checked="checked"@endif>
                                                Ativos
                                            </label>
                                            <label for="inativo" class="send_left" style="padding-right: 20px;">
                                                <input type="radio" name="status_district" id="inativo" value="0" @if ($context->filtros && $context->filtros['status'] == 0)checked="checked"@endif>
                                                Inativos
                                            </label>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>

                        <div class="button_bar clearfix">
                            <button class="green dark img_icon has_text" id="filtrar">
                                <img src="/admin/images/icons/small/white/bended_arrow_right.png">
                                <span>Filtrar</span>
                            </button>

                            <button class="grey dark send_right img_icon has_text" id="resetar">
                                <img src="/admin/images/icons/small/white/bended_arrow_left.png">
                                <span>resetar filtros</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            @if ($context->filtros != null)
            <div class="box grid_16 single_datatable">
                <h2 class="box_head">Resultado</h2>
                <div class="block">
                    <table class="static">
                        <thead>
                        <tr>
                            <th>Bairro</th>
                            <th>Localização</th>
                            <th>Empresas</th>
                            <th>Slug</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @if (count($context->bairros) > 0)
                            @foreach($context->bairros as $bairro)
                            <tr>
                                <td class="hightlight_td">{{ $bairro->name }}</td>
                                <td>{{ $bairro->city->name }} - {{ $bairro->city->estate->short }}</td>
                                <td><span class="status status-success">
                                        {{ $bairro->qtde_empresas() }} - <a href="/padmin/business/search/{{ $bairro->city->estate->id }}/{{ $bairro->city->id }}/{{ $bairro->id }}/-1/-1/-1">exibir</a>
                                    </span>
                                </td>
                                <td>{{ $bairro->slug }}</td>
                                <td style="text-align: right;">
                                    @if ($bairro->active)
                                    <span class="status status-important"><a href="/padmin/geo/district/deactivate/{{ $bairro->id }}">desativar</a></span>
                                    @else
                                    <span class="status status-success"><a href="/padmin/geo/district/activate/{{ $bairro->id }}">ativar</a></span>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        @else
                        <tr>
                            <td colspan="4">Nenhum bairro encontrada</td>
                        </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            {{ $context->bairros->links() }}
            @endif

        </div>

        <div id="loading_overlay">
            <div class="loading_message round_bottom">
                <img src="/admin/images/interface/loading.gif" alt="loading" />
            </div>
        </div>
    </div>
</div>
@endsection