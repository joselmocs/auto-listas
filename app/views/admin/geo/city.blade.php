@extends('admin/template')

@section('scripts')
{{ HTML::script('/admin/scripts/geo/base.js') }}
@endsection

@section('styles')
{{ HTML::style('/admin/styles/custom.css') }}
@endsection

@section('conteudo')
<div id="pjax">
    <div id="wrapper" data-adminica-nav-top="1" data-adminica-side-top="1">

        @include('admin/include_user_box')

        <div id="main_container" class="main_container container_16 clearfix">

            @include('admin/include_menu')

            <div class="flat_area grid_16">
                <h2>@if ($context->estado){{ $context->estado->name }}@else Lista de Municípios@endif</h2>
                <p>Empresas cadastradas em municípios desativados <strong>não</strong> irão aparecer nos resultados de busca no site.</p>
                @if ($context->estado != null)
                <button class="blue send_right text_only has_text" type="button" id="selecionar_outro_estado">
                    <img src="/admin/images/icons/small/white/globe_2.png">
                    <span>Selecionar outro estado</span>
                </button>
                @endif
            </div>

            @if ($context->estado == null)
            <div class="box grid_16 no_titlebar">
                <div class="block">
                    <h2 class="section">Para continuar selecione um estado</h2>

                    <form class="validate_form" novalidate="novalidate">
                        <fieldset>
                            <div class="clearfix">
                                <select class="select3 required" style="width: 100%;" id="select_estado">
                                    <optgroup label="Selecione um estado">
                                    @foreach ($context->estados as $estado)
                                    <option value="{{ strtolower($estado->short) }}">{{ $estado->name }}</option>
                                    @endforeach
                                    </optgroup>
                                </select>
                            </div>
                        </fieldset>

                        <div class="button_bar clearfix">
                            <button class="green img_icon has_text" type="button" id="button_select_estado">
                                <img height="24" width="24" alt="Bended Arrow Right" src="/admin/images/icons/small/white/bended_arrow_right.png">
                                <span>Selecionar</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            @else

            <div class="jqui_radios ui-buttonset" style="padding-left: 9px;">
                <input type="radio" name="filter" class="ui-helper-hidden-accessible" id="f_a" checked="true" value="a">
                    <label for="f_a" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-state-active" aria-pressed="true" role="button" aria-disabled="false">
                        <span>A</span>
                    </label>
                <input type="radio" name="filter" class="ui-helper-hidden-accessible" id="f_b" value="b">
                    <label for="f_b" aria-pressed="false" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false">
                        <span>B</span>
                    </label>
                <input type="radio" name="filter" class="ui-helper-hidden-accessible" id="f_c" value="c">
                    <label for="f_c" aria-pressed="false" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false">
                        <span>C</span>
                    </label>
                <input type="radio" name="filter" class="ui-helper-hidden-accessible" id="f_d" value="d">
                    <label for="f_d" aria-pressed="false" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false">
                        <span>D</span>
                    </label>
                <input type="radio" name="filter" class="ui-helper-hidden-accessible" id="f_e" value="e">
                    <label for="f_e" aria-pressed="false" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false">
                        <span>E</span>
                    </label>
                <input type="radio" name="filter" class="ui-helper-hidden-accessible" id="f_f" value="f">
                    <label for="f_f" aria-pressed="false" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false">
                        <span>F</span>
                    </label>
                <input type="radio" name="filter" class="ui-helper-hidden-accessible" id="f_g" value="g">
                    <label for="f_g" aria-pressed="false" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false">
                        <span>G</span>
                    </label>
                <input type="radio" name="filter" class="ui-helper-hidden-accessible" id="f_h" value="h">
                    <label for="f_h" aria-pressed="false" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false">
                        <span>H</span>
                    </label>
                <input type="radio" name="filter" class="ui-helper-hidden-accessible" id="f_i" value="i">
                    <label for="f_i" aria-pressed="false" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false">
                        <span>I</span>
                    </label>
                <input type="radio" name="filter" class="ui-helper-hidden-accessible" id="f_j" value="j">
                    <label for="f_j" aria-pressed="false" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false">
                        <span>J</span>
                    </label>
                <input type="radio" name="filter" class="ui-helper-hidden-accessible" id="f_k" value="k">
                    <label for="f_k" aria-pressed="false" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false">
                        <span>K</span>
                    </label>
                <input type="radio" name="filter" class="ui-helper-hidden-accessible" id="f_l" value="l">
                    <label for="f_l" aria-pressed="false" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false">
                        <span>L</span>
                    </label>
                <input type="radio" name="filter" class="ui-helper-hidden-accessible" id="f_m" value="m">
                    <label for="f_m" aria-pressed="false" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false">
                        <span>M</span>
                    </label>
                <input type="radio" name="filter" class="ui-helper-hidden-accessible" id="f_n" value="n">
                    <label for="f_n" aria-pressed="false" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false">
                        <span>N</span>
                    </label>
                <input type="radio" name="filter" class="ui-helper-hidden-accessible" id="f_o" value="o">
                    <label for="f_o" aria-pressed="false" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false">
                        <span>O</span>
                    </label>
                <input type="radio" name="filter" class="ui-helper-hidden-accessible" id="f_p" value="p">
                    <label for="f_p" aria-pressed="false" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false">
                        <span>P</span>
                    </label>
                <input type="radio" name="filter" class="ui-helper-hidden-accessible" id="f_q" value="q">
                    <label for="f_q" aria-pressed="false" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false">
                        <span>Q</span>
                    </label>
                <input type="radio" name="filter" class="ui-helper-hidden-accessible" id="f_r" value="r">
                    <label for="f_r" aria-pressed="false" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false">
                        <span>R</span>
                    </label>
                <input type="radio" name="filter" class="ui-helper-hidden-accessible" id="f_s" value="s">
                    <label for="f_s" aria-pressed="false" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false">
                        <span>S</span>
                    </label>
                <input type="radio" name="filter" class="ui-helper-hidden-accessible" id="f_t" value="t">
                    <label for="f_t" aria-pressed="false" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false">
                        <span>T</span>
                    </label>
                <input type="radio" name="filter" class="ui-helper-hidden-accessible" id="f_u" value="u">
                    <label for="f_u" aria-pressed="false" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false">
                        <span>U</span>
                    </label>
                <input type="radio" name="filter" class="ui-helper-hidden-accessible" id="f_v" value="v">
                    <label for="f_v" aria-pressed="false" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false">
                        <span>V</span>
                    </label>
                <input type="radio" name="filter" class="ui-helper-hidden-accessible" id="f_w" value="w">
                    <label for="f_w" aria-pressed="false" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false">
                        <span>W</span>
                    </label>
                <input type="radio" name="filter" class="ui-helper-hidden-accessible" id="f_x" value="x">
                    <label for="f_x" aria-pressed="false" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false">
                        <span>X</span>
                    </label>
                <input type="radio" name="filter" class="ui-helper-hidden-accessible" id="f_y" value="y">
                    <label for="f_y" aria-pressed="false" class="ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false">
                        <span>Y</span>
                    </label>
                <input type="radio" name="filter" class="ui-helper-hidden-accessible" id="f_z" value="z">
                    <label for="f_z" aria-pressed="false" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-right" role="button" aria-disabled="false">
                        <span>Z</span>
                    </label>
            </div>

            <div class="box grid_16 single_datatable"></div>

            <script type="text" id="template_table" class="display_none">
                <div id="" class="no_margin">
                    <table class=" datatable">
                        <thead>
                        <tr>
                            <th>Município</th>
                            <th>Slug</th>
                            <th>Empresas</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody class="cities_content"></tbody>
                    </table>
                </div>
            </script>

            <script type="text" class="display_none" id="template_cities">
                <tr>
                    <td>{cidade}</td>
                    <td style="text-align: center;">{slug}</td>
                    <td style="text-align: center;">{empresas}</td>
                    <td style="text-align: right;">{status}</td>
                </tr>
            </script>
            @endif
        </div>

        <div id="loading_overlay">
            <div class="loading_message round_bottom">
                <img src="/admin/images/interface/loading.gif" alt="loading" />
            </div>
        </div>
    </div>
</div>

@endsection