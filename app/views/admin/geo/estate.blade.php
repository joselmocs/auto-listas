@extends('admin/template')

@section('styles')
{{ HTML::style('/admin/styles/custom.css') }}
@endsection

@section('conteudo')
<div id="pjax">
    <div id="wrapper" data-adminica-nav-top="1" data-adminica-side-top="1">

        @include('admin/include_user_box')

        <div id="main_container" class="main_container container_16 clearfix">

            @include('admin/include_menu')

            <div class="flat_area grid_16">
                <h2>Lista de Estados</h2>
                <p>Cidades e empresas cadastradas em estados desativados <strong>não</strong> irão aparecer nos resultados de busca no site.</p>
            </div>

            <div class="box grid_16">

                <div class="block">
                    <table class="static">
                        <thead>
                        <tr>
                            <th><strong>Sigla</strong></th>
                            <th><strong>Nome</strong></th>
                            <th><strong>Slug</strong></th>
                            <th style="width: 70px;"><strong>Cidades</strong></th>
                            <th style="width: 60px;text-align: right;"><strong>Status</strong></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($context->estados as $estado)
                        <tr>
                            <td>{{ $estado->short }}</td>
                            <td>{{ $estado->name }}</td>
                            <td>{{ $estado->slug }}</td>
                            <td>{{ $estado->qtde_cidades() }}</td>
                            <td style="text-align: right;">
                                @if ($estado->active)
                                <span class='status status-important' onclick="window.location='/padmin/geo/estate/deactivate/{{ $estado->id }}'"><a href="javascript:void(0);">Desativar</a></span>
                                @else
                                <span class='status status-success' onclick="window.location='/padmin/geo/estate/activate/{{ $estado->id }}'"><a href="javascript:void(0);">Ativar</a></span>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

        <div id="loading_overlay">
            <div class="loading_message round_bottom">
                <img src="/admin/images/interface/loading.gif" alt="loading" />
            </div>
        </div>
    </div>
</div>

@endsection