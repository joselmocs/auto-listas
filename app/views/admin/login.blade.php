@extends('admin/template')

@section('conteudo')
<div id="pjax">
<div id="wrapper">
<div class="isolate">
    <div class="center narrow">
        <div class="main_container full_size container_16 clearfix">
            <div class="box">
                <div class="block">
                    <div class="section">
                        <div class="alert dismissible alert_light">
                            <img width="24" height="24" src="/admin/images/icons/small/grey/locked.png">
                            <strong>{{ Config::get('server.site.name') }}.</strong> Por favor, informe seus dados de login.
                        </div>
                        @if (count($errors) > 0)
                            @foreach ($errors->all() as $e)
                                <div class="alert dismissible alert_red">
                                    <img height="24" width="24" src="/admin/images/icons/small/white/locked_2.png">
                                    {{ $e }}
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <form action="/padmin/login" method="post" class="validate_form">
                        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
                        <fieldset class="label_side top">
                            <label for="username_field">Usuário<span>ou endereço e-mail</span></label>
                            <div>
                                <input type="text" name="username_field" class="required">
                            </div>
                        </fieldset>
                        <fieldset class="label_side bottom">
                            <label for="password_field">Senha</label>
                            <div>
                                <input type="password" name="password_field" class="required">
                            </div>
                        </fieldset>
                        <div class="button_bar clearfix">
                            <button class="wide" type="submit">
                                <img src="/admin/images/icons/small/white/key_2.png">
                                <span>Entrar</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <a href="#" id="login_logo"><span>Adminica</span></a>
    </div>
</div>

<div id="loading_overlay">
    <div class="loading_message round_bottom">
        <img src="/admin/images/interface/loading.gif" alt="loading" />
    </div>

</div>
</div>
@endsection