<div id="nav_top" class="dropdown_menu clearfix round_top">
    <ul class="clearfix">
        <li class="icon_only @if ($context->menu_atual[0] == 'index') current @endif">
            <a href="/padmin"><img src="/admin/images/icons/small/grey/laptop.png"/><span class="display_none">Inicial</span></a>
        </li>
        <li class="@if ($context->menu_atual[0] == 'business') current @endif">
            <a href="#"><img src="/admin/images/icons/small/grey/frames.png"/><span>Empresas</span></a>
            <ul>
                <li class="@if ($context->menu_atual[1] == 'business') current @endif"><a href="/padmin/business"><span>Empresas</span></a></li>
                <li class="@if ($context->menu_atual[1] == 'category') current @endif"><a href="/padmin/business/category"><span>Categorias</span></a></li>
                <li class="@if ($context->menu_atual[1] == 'tag') current @endif"><a href="/padmin/business/tag"><span>Etiquetas</span></a></li>
            </ul>
        </li>
        <li class="@if ($context->menu_atual[0] == 'geo') current @endif">
            <a href="#"><img src="/admin/images/icons/small/grey/globe.png"/><span>Geo</span></a>
            <ul class="open_multiple">
                <li class="@if ($context->menu_atual[1] == 'district') current @endif"><a href="/padmin/geo/district"><span>Bairros</span></a></li>
                <li class="@if ($context->menu_atual[1] == 'city') current @endif"><a href="/padmin/geo/city"><span>Municípios</span></a></li>
                <li class="@if ($context->menu_atual[1] == 'estate') current @endif"><a href="/padmin/geo/estate"><span>Estados</span></a></li>
            </ul>
        </li>

        <li class="@if ($context->menu_atual[0] == 'user') current @endif">
            <a href="#"><img src="/admin/images/icons/small/grey/users_2.png"/><span>Pessoas</span></a>
            <ul class="open_multiple">
                <li class="@if ($context->menu_atual[1] == 'group') current @endif"><a href="/padmin/user/group"><span>Grupos</span></a></li>
                <li class="@if ($context->menu_atual[1] == 'system') current @endif"><a href="/padmin/user/system"><span>Sistema</span></a></li>
                <li class="@if ($context->menu_atual[1] == 'all') current @endif"><a href="/padmin/user/all"><span>Lista de Usuários</span></a></li>
            </ul>
        </li>
        <li>
        <li><a href="/padmin/logout"><img src="/admin/images/icons/small/grey/exit.png"/><span>Sair</span></a></li>
        </li>
    </ul>
</div>