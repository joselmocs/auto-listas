@extends('admin/template')

@section('styles')
{{ HTML::style('/admin/styles/custom.css') }}
@endsection

@section('scripts')
{{ HTML::script('/admin/scripts/category/base.js') }}
@endsection

@section('conteudo')
<div id="pjax">
    <div id="wrapper" data-adminica-nav-top="1" data-adminica-side-top="1">

        @include('admin/include_user_box')

        <div id="main_container" class="main_container container_16 clearfix">

            @include('admin/include_menu')

            <div class="flat_area grid_16">
                <h2>Lista de Categorias</h2>
            </div>

            <div class="box grid_10 single_datatable">
                <div id="dt1" class="no_margin">
                    <table class="datatable">
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Slug</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($context->categorias as $category)
                        <tr>
                            <td>{{ $category->name }}</td>
                            <td>{{ $category->slug }}</td>
                            <td style="width: 60px;text-align: right;">
                                <input type="hidden" name="data-tag" value="@foreach($category->tags as $tag){{ $tag->id }},@endforeach">
                                <span class='pointer status status-warning' onclick="Category.mode.edit('{{ $category->name }}', {{ $category->id }}, $(this))">Editar</span>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="box grid_6 no_titlebar">
                <div class="block">
                    <div class="section" name="mode_new"><h2>Adicionar nova Categoria</h2></div>
                    <div class="section" name="mode_edit" style="display: none;"><h2></h2></div>

                    <div id="error_add_category" style="padding: 0 20px;display: none;">
                        <div class="alert alert_red">
                            <img height="24" width="24" src="/admin/images/icons/small/white/alert_2.png">
                            <span name="msg_add_category">Por favor, preencha o nome da categoria.</span>
                        </div>
                    </div>

                    <div class="columns clearfix">
                        <div>
                            <fieldset class="label_top top">
                                <label for="tag_field">Nome<span>O nome é como aparece no site.</span></label>
                                <div>
                                    <input type="text" class="text" id="category_field" class="required">
                                    <div class="required_tag hover left"></div>
                                </div>
                            </fieldset>
                        </div>
                    </div>

                    <div class="columns clearfix">
                        <div>
                            <fieldset class="label_top top">
                                <label for="tag_field">Etiquetas</label>
                                <div id="container-select">
                                    <select class="select3" style="width: 284px;" id="tags" multiple="multiple">
                                        @foreach($context->etiquetas as $tag)
                                        <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </fieldset>
                        </div>
                    </div>

                    <div class="button_bar clearfix">
                        <div name="mode_new">
                            <button class="dark blue no_margin_bottom div_icon has_text" type="button" id="button_add_category">
                                <div class="ui-icon ui-icon-check"></div>
                                <span>Adicionar nova Categoria</span>
                            </button>
                        </div>
                        <div name="mode_edit" style="display: none;">
                            <button class="dark blue no_margin_bottom div_icon has_text" type="button" id="button_edit_category">
                                <div class="ui-icon ui-icon-check"></div>
                                <span>Editar</span>
                            </button>

                            <button class="light no_margin_bottom div_icon has_text" type="button" id="button_mode_new">
                                <div class="ui-icon ui-icon-closethick"></div>
                                <span>Cancelar</span>
                            </button>

                            <button class="dark red send_right no_margin_bottom div_icon has_text dialog_button" data-dialog="dialog_remove_category" type="button">
                                <div class="ui-icon ui-icon-closethick"></div>
                                <span>Remover</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div id="loading_overlay">
            <div class="loading_message round_bottom">
                <img src="/admin/images/interface/loading.gif" alt="loading" />
            </div>
        </div>
    </div>
</div>

<div class="display_none">
    <div id="dialog_remove_category" class="dialog_content no_dialog_titlebar narrow " title="">
        <div class="block">
            <div class="section">
                <h2>Remover Categoria</h2>
                <div class="dashed_line"></div>
                Ao remover esta categoria ela também será removida das empresas.
            </div>
            <div class="button_bar clearfix">
                <button class="dark red no_margin_bottom" onclick="Category.remove();">
                    <div class="ui-icon ui-icon-check"></div>
                    <span>Remover</span>
                </button>
                <button class="light send_right close_dialog">
                    <div class="ui-icon ui-icon-closethick"></div>
                    <span>Cancelar</span>
                </button>
            </div>
        </div>
    </div>
</div>

@endsection