@extends('admin/template')

@section('scripts')
{{ HTML::script('/admin/scripts/user/system.js') }}
@endsection

@section('styles')
{{ HTML::style('/admin/styles/custom.css') }}
@endsection

@section('conteudo')
<div id="pjax">
    <div id="wrapper" data-adminica-nav-top="1" data-adminica-side-top="1">

        @include('admin/include_user_box')

        <div id="main_container" class="main_container container_16 clearfix">

            @include('admin/include_menu')

            <div class="flat_area grid_16">
                <h2>Lista de Usuários do Sistema</h2>
                <p>Lista de usuários que tem alguma permissão especial no site ou na administração.</p>

                <button class="send_right green dark img_icon has_text" id="filtrar">
                    <img src="/admin/images/icons/small/white/bended_arrow_right.png">
                    <span>Adicionar usuário</span>
                </button>
            </div>

            <div class="box grid_16">

                <div class="block">
                    <table class="static">
                        <thead>
                        <tr>
                            <th><strong>Usuário</strong></th>
                            <th><strong>Nome</strong></th>
                            <th><strong>Email</strong></th>
                            <th><strong>Grupo</strong></th>
                            <th style="width: 60px;"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($context->usuarios as $user)
                        <tr>
                            <td>{{ $user->username }}</td>
                            <td>{{ $user->profile->firstname }} {{ $user->profile->lastname }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->group() }}</td>
                            <td style="text-align: right;">
                                <span class='status status-warning'><a href="/padmin/user/system/edit/{{ $user->id }}">Editar</a></span>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

        <div id="loading_overlay">
            <div class="loading_message round_bottom">
                <img src="/admin/images/interface/loading.gif" alt="loading" />
            </div>
        </div>
    </div>
</div>
@endsection