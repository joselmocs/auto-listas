<div id="topbar" class="clearfix">
    <a href="/padmin" class="logo"><span>{{ Config::get('server.site.name') }}</span></a>
    <div class="user_box dark_box clearfix">
        <img src="/admin/images/interface/profile.jpg" width="55" alt="Profile Pic" />
        <h2>{{ Auth::user()->group() }}</h2>
        <h3><a class="text_shadow" href="/padmin/user/{{ strtolower(Auth::user()->username) }}">{{ Auth::user()->profile->firstname }} {{ Auth::user()->profile->lastname }}</a></h3>
        <ul>
            <li><a href="/padmin/user/{{ strtolower(Auth::user()->username) }}">meu perfil</a><span class="divider">|</span></li>
            <li><a href="/padmin/logout">Sair</a></li>
        </ul>
    </div>
</div>