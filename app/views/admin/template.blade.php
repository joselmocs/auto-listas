<!doctype html>
<!--[if lt IE 7]><html lang="pt-br" class="no-js ie6"> <![endif]-->
<!--[if IE 7]><html lang="pt-br" class="no-js ie7"> <![endif]-->
<!--[if IE 8]><html lang="pt-br" class="no-js ie8"> <![endif]-->
<!--[if IE 9]><html lang="pt-br" class="no-js ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="pt-br" class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">

    <title>{{ Config::get('server.site.name') }} :: Backend</title>

    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="/admin/styles/adminica/reset.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,700">

    <link rel="stylesheet" href="/admin/styles/plugins/plugins.css">
    <link rel="stylesheet" href="/admin/styles/adminica/all.css">

    <link rel="stylesheet" href="/admin/styles/themes/layout_fixed.css" >
    <link rel="stylesheet" href="/admin/styles/themes/nav_top.css" >
    <link rel="stylesheet" href="/admin/styles/themes/switcher.css" >
    <link rel="stylesheet" href="/admin/styles/themes/theme_blue.css" >
    <link rel="stylesheet" href="/admin/styles/themes/bg_noise.css" >

    <link rel="stylesheet" href="/admin/styles/adminica/colours.css">

    <link rel="stylesheet" href="/admin/styles/select3.css">

    <script type="text/javascript" src="/admin/scripts/plugins-min.js"></script>

    <script type="text/javascript" src="/admin/scripts/adminica/adminica_ui.js"></script>
    <script type="text/javascript" src="/admin/scripts/adminica/adminica_datatables.js"></script>
    <script type="text/javascript" src="/admin/scripts/adminica/adminica_calendar.js"></script>
    <script type="text/javascript" src="/admin/scripts/adminica/adminica_charts.js"></script>
    <script type="text/javascript" src="/admin/scripts/adminica/adminica_gallery.js"></script>
    <script type="text/javascript" src="/admin/scripts/adminica/adminica_various.js"></script>
    <script type="text/javascript" src="/admin/scripts/adminica/adminica_wizard.js"></script>
    <script type="text/javascript" src="/admin/scripts/adminica/adminica_forms.js"></script>
    <script type="text/javascript" src="/admin/scripts/adminica/adminica_load.js"></script>

    <script type="text/javascript" src="/admin/scripts/select3.js"></script>

    @yield('styles')

</head>
<body>

@yield('conteudo')

<script type="text/javascript">
//<![CDATA[
var _context = {{ $json }};
//]]>
</script>

@yield('scripts')

</body>
</html>