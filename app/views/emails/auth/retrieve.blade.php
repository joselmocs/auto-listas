@if ($tipo_email == "password")
<p>Olá {{ $user->profile->firstname }},</p>
<p>Foi solicitado a recuperação da senha para o usuário <b>{{ strtolower($user->username) }}</b> no site {{ $site_name }}.</p>
<p>Para criar uma nova senha clique no link a abaixo ou copie e cole no seu navegador:<br />
    {{ $url }}/user/retrieve/{{ strtolower($user->username) }}/{{ $user->salt }}</p>
<p>Se você não solicitou uma nova senha apenas ignore este email.</p>
@endif

@if ($tipo_email == "password_changed")
<p>Olá {{ $user->profile->firstname }},</p>
<p>Esta é uma notificação automática enviada sobre mudança(s) recente(s) feitas em sua conta: {{ strtolower($user->email) }}</p>
<p>Sua senha foi alterada recentemente. Se você realizou esta alteração, ignore esta notificação.</p>
<p>Se você não realizou esta mudança de senha, visite o site abaixo para alterar sua senha e assegurar que sua conta esteja segura:<br />
{{ $url }}/user/retrieve/{{ strtolower($user->username) }}/{{ $user->salt }}</p>
@endif

<p>Cordialmente,<br />
Equipe {{ $site_name }}</p>