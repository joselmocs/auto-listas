<!doctype html>
<html lang="pt-br">
<meta charset="utf-8" />
<head>
    <meta charset="UTF-8">
    <title>@yield('page_title')</title>

    {{ HTML::style('/styles/global.css') }}
    @yield('styles')
</head>

<body class="@yield('body_class')">

<div id="global-header">
    <div class="header-navigation">
        <div class="panel">
            <ul class="nav-links">
                <li><a href="#" rel="nofollow">Anuncie</a></li>
                <li id="advertise-link"><a href="#" rel="nofollow">Minha Empresa</a></li>
            </ul>
            <div id="about-us-link">
                <a href="#" title="About YP.com">Quem Somos</a>
            </div>
        </div>
        <div class="clear"></div>
    </div>

    <div class="header-panel" style="@if ($context->inicial)background-color: #D9D8D0; background-image: url(/images/header/bg1.jpg); @endif">
        <div class="panel">
            <div class="header-branding">
                <a href="/" class="yp-logo">AutoListas.com.br</a>
            </div>
            <div class="header-search">
                <form action="/search" class="search-form" method="get">
                    <input type="hidden" name="tmatchid" value="">
                    <input type="hidden" name="lmatchid" value="">
                    <input type="hidden" name="lmatchtype" value="">
                    <div class="search-form">
                        <div class="label">
                            <input data-suggest_type="autosuggest" id="search-terms" maxlength="100" name="search_terms" tabindex="1" type="text" value="" class="ui-autocomplete-input" placeholder="O que está procurando?">
                        </div>
                        <p class="near">em</p>
                        <div class="label">
                            <input id="search-location" tabindex="2" type="text" value="" class="ui-autocomplete-input" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" placeholder="Onde?">
                        </div>
                        <input class="findbutton" id="search-submit" tabindex="3" type="submit" value="Procurar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="layout">
    <div class="panel" id="container">
        @yield('conteudo')
    </div>
</div>

<div id="global-footer">
    <div class="panel" id="site-categories">
        <div class="site-category" id="footer-site-about">
            <h2>About</h2>
            <ul>
                <li><a href="#" class="no-tracks" title="About YP.com">About YP</a></li>
                <li><a href="#" class="no-tracks" title="Contact Us">Contact Us</a></li>
                <li><a href="#" class="no-tracks" title="Contact Us">Privacy Policy</a></li>
            </ul>
        </div>
        <div class="site-category" id="footer-site-about">
            <h2>About</h2>
            <ul>
                <li><a href="#" class="no-tracks" title="About YP.com">About YP</a></li>
                <li><a href="#" class="no-tracks" title="Contact Us">Contact Us</a></li>
                <li><a href="#" class="no-tracks" title="Contact Us">Privacy Policy</a></li>
            </ul>
        </div>
        <div class="site-category" id="footer-site-about">
            <h2>About</h2>
            <ul>
                <li><a href="#" class="no-tracks" title="About YP.com">About YP</a></li>
                <li><a href="#" class="no-tracks" title="Contact Us">Contact Us</a></li>
                <li><a href="#" class="no-tracks" title="Contact Us">Privacy Policy</a></li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
    <p id="copyright">
        © 2013 YP Intellectual Property LLC. All rights reserved.
        <br>
        YP, the YP logo and all other YP marks contained herein are trademarks of YP Intellectual Property LLC and/or YP affiliated companies.
        <br>
        AT&amp;T, the AT&amp;T Logo and all AT&amp;T related marks are trademarks of AT&amp;T Inc. or AT&amp;T affiliated companies.
        All other marks contained herein are the property of their respective owners.
    </p>
</div>

<script type="text/javascript">
//<![CDATA[
var _context = {{ $json }};
//]]>
</script>
<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
{{ HTML::script('http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js') }}
{{ HTML::script('/scripts/plugins/jquery.ui.min.js') }}
{{ HTML::script('/scripts/al.autocomplete.js') }}
{{ HTML::script('/scripts/search.js') }}
@yield('scripts')

</body>
</html>