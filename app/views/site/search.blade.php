@extends('template')

@section('page_title')
{{ Config::get('server.site.title') }}
@endsection

@section('styles')
{{ HTML::style('/styles/search.css') }}
@endsection

@section('body_class')search_results @endsection

@section('conteudo')
<div class="search show" id="canvas">
    <div>
        <h1>Chicago Auto repair service</h1>
        <p class="result-count">Results 1-30 of 1625</p>
    </div>
    <div class="clear"></div>

    <div class="section" id="left-column">

        <div class="clearfix">
            <a class="show-filters filter-toggle" href="#" style="display: block;">
                <span class="arrow-closed"></span>
                Filters
            </a>
        </div>


        <div class="results-column">
            <div class="refinement-overlay">&nbsp;</div>
            <div class="clearfix">
                <div class="clearfix" id="sponsored-listings">
                    <h2>Featured Businesses</h2>
                    <ul>
                        <li class="clearfix first pll tier-20 vcard">
                            <span class="biz_graphic">
                                <a href="/" target="_blank"><img alt="" border="0" class="listing-ad-image no-tracks" src="http://i1.ypcdn.com/displaygifResize/rd/S2468060.gif"></a>
                            </span>
                            <div class="track-listing" id="topwell-lid-451047029">
                                <span class="mapped"></span>
                                <h3 class="title fn org">
                                    <a href="#" class="url" rel="nofollow" title="South Chicago Dodge">South Chicago Dodge</a>
                                </h3>
                                <br>
                                <span class="listing-address adr">
                                    <span class="street-address">7340 S Western Ave,</span>
                                    <span class="city-state">
                                        <span class="locality">Chicago</span>, <span class="region">IL</span>
                                        <span class="postal-code">60636</span>
                                    </span>
                                </span>
                                <span class="phone">(773) 476-7800</span>
                                <ul class="features">
                                    <li class="website-feature">
                                        <a href="/" class="track-visit-website" rel="nofollow" target="_blank" title="South Chicago Dodge Website"><span class="raquo">»</span>  Website</a>
                                    </li>
                                    <li class="coupon-feature">
                                        <a href="/" class="track-coupon coupon-link" rel="nofollow" title="South Chicago Dodge coupon"><span class="raquo">»</span> Coupons</a>
                                    </li>
                                    <li class="print_ad-feature">
                                        <a href="/" class="track-print-ad" rel="nofollow" title="Yellow Pages print advertisement for South Chicago Dodge"><span class="raquo">»</span> YP Ad</a>
                                    </li>
                                    <li class="more_info-feature">
                                        <a href="/" class="track-more-info" rel="nofollow"><span class="raquo">»</span> More Info</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div id="search-results">
                <div id="search-content">
                    <div id="results">


                        @foreach($context->resultado as $bus)

                        <div class="clearfix paid-listing result track-listing vcard">
                            <div class="listing-content">
                                <div class="rank pin">1</div>
                                <div class="info">
                                    <div class="clearfix">
                                        <h3 class="business-name fn org" data-lid="1729471">
                                            <div class="srp-business-name">
                                                <a href="/" class="url " title="Marvin's Auto Repair">{{ $bus->name }}</a>
                                            </div>
                                        </h3>
                                    </div>
                                    @if ($bus->thumb)
                                    <div class="thumbnail photo">
                                        <a href="/" class="track-view-photo no-tracks" rel="nofollow" title=" Marvin's Auto Repair - View more photos">
                                            <img alt="Photo"  class="no-tracks" src="{{ $bus->thumb }}">
                                        </a>
                                    </div>
                                    @endif
                                    <span class="listing-address adr">
                                        <span class="street-address">{{ $bus->address1 }}</span><br />
                                        <span class="street-address">{{ $bus->address2 }}</span>
                                    </span>
                                    <!--<a href="/" class="map-link" rel="nofollow"><span class="raquo">»</span> Map</a>-->

                                    <span class="business-phone phone">{{ $bus->phone }}</span>
                                </div>
                                <div class="result-rating">
                                    <div class="rating write-review-stars">
                                        <p class="average rating-0"></p>
                                    </div>
                                    <p class="review-count">
                                        (<a href="/" class="count track-read-reviews" rel="nofollow">{{ $bus->rating_votes }}</a>)
                                    </p>
                                </div>


                                @if ($bus->website)
                                <ul class="features">
                                    <li class="website-feature">
                                        <a href="/" class="track-visit-website" rel="nofollow" target="_blank" title="Marvin's Auto Repair Website"><span class="raquo">»</span>  Website</a>
                                    </li>
                                    <li class="more_info-feature">
                                        <a href="/" class="track-more-info" rel="nofollow"><span class="raquo">»</span> More Info</a>
                                    </li>
                                </ul>
                                @endif
                                <div class="what-where">
                                    <div class="categories">
                                        Categorias:
                                        <ul class="business-categories">
                                            @foreach ($bus->categories as $category)
                                            <li><a href="/" rel="nofollow">{{ $category->name }}</a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="listing-actions" style="">
                                <div class="social-share-box">
                                    <ul>
                                        <li><a href="/" alt="Email this information" class="share-email track-email-it email-lightbox-link no-tracks" rel="nofollow" title="Email this information">Email</a></li>
                                        @if ($bus->facebook)
                                        <li><a href="/" alt="Share on Facebook" class="social-external-site track-facebook logo" rel="nofollow" title="Share on Facebook">Facebook</a></li>
                                        @endif
                                        @if ($bus->twitter)
                                        <li><a href="/" alt="Share on Twitter" class="social-external-site track-twitter logo" rel="nofollow" title="Share on Twitter">Twitter</a></li>
                                        @endif
                                    </ul>
                                </div>
                                <span class="inaccuracy">
                                    <a href="/" rel="nofollow">Inaccurate result?</a>
                                </span>
                            </div>
                        </div>

                        @endforeach

                        <div class="clearfix" id="related-searches">
                            <h2>
                                <span class="heading-orange">Related Searches</span>
                                <span class="related-params">for <span class="search-terms">Auto repair service</span> in <span class="search-geo">Chicago, IL</span></span>
                            </h2>
                            <ul>
                                <li><a href="/" class="no-tracks"><span class="matched-terms">auto</span> <span class="matched-terms">repair</span></a></li>
                                <li><a href="/" class="no-tracks"><span class="matched-terms">auto</span>mobile body <span class="matched-terms">repair</span>ing painting</a></li></ul>
                            <ul>
                                <li><a href="/" class="no-tracks"><span class="matched-terms">auto</span>mobile parts supplies</a></li>
                                <li><a href="/" class="no-tracks">tire dealers</a></li></ul>
                            <ul>
                                <li><a href="/" class="no-tracks">brake <span class="matched-terms">repair</span></a></li>
                                <li><a href="/" class="no-tracks">towing</a></li></ul>
                        </div>


                        <div class="pagination">
                            <div class="page-navigation">
                                <ol class="track-pagination">
                                    <li class="disabled">
                                        Previous
                                    </li>
                                    <li class="current">
                                        1
                                    </li>
                                    <li>
                                        <a href="/">2</a>
                                    </li>
                                    <li>
                                        <a href="/">3</a>
                                    </li>
                                    <li>
                                        <a href="/">4</a>
                                    </li>
                                    <li>
                                        <a href="/">5</a>
                                    </li>
                                    <li class="next">
                                        <a href="/">Next</a>
                                    </li>
                                </ol>
                            </div>
                            <div class="result-totals">
                                Showing results for Chicago Auto repair service:
                                <strong>1-30</strong>
                                of
                                <strong>1625</strong>
                            </div>
                        </div>

                        <p class="add-listing">
                            If we're missing a business and you'd like to make a suggestion, please do!
                            <a href="/" class="ugli" target="_blank">Add a business »</a>
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section" id="right-column">
        <div class="featured">
            <h2>Featured Auto Repair &amp; Service in Chicago, IL</h2>
            <div>
                <div class="sb">
                    <div class="sb-group">
                        <ul class="diamond">
                            <li class="clearfix diamond first tier-25 track-listing vcard">
                                <div class="business-info result">
                                    <span class="mapped"></span>
                                    <h3 class="business-title fn org">
                                        <a href="/" class="url " rel="nofollow" title="Rosen Motor Sales">Rosen Motor Sales</a>
                                    </h3>
                                    <span class="listing-address adr">
                                        <span class="street-address">7000 Grand Ave,</span>
                                        <span class="city-state">
                                            <span class="locality">Gurnee</span>,
                                            <span class="region">IL</span>
                                            <span class="postal-code">60031</span>
                                        </span>
                                    </span>
                                    <span class="phone">(847) 623-7673</span>
                                </div>

                                <span class="biz_graphic">
                                <a href="/" target="_blank"><img alt="" border="0" class="listing-ad-image no-tracks" src="http://i1.ypcdn.com/displaygifResize/rd/S6477965.gif"></a>
                                </span>

                                <ul class="features">
                                    <li class="website-feature">
                                        <a href="/" class="track-visit-website"><span class="raquo">»</span>  Website</a>
                                    </li>
                                    <li class="more_info-feature">
                                        <a href="/" class="track-more-info" rel="nofollow"><span class="raquo">»</span> More Info</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="diamond">
                            <li class="clearfix diamond first tier-25 track-listing vcard">
                                <div class="business-info result">
                                    <span class="mapped"></span>
                                    <h3 class="business-title fn org">
                                        <a href="/" class="url " rel="nofollow" title="Rosen Motor Sales">Rosen Motor Sales</a>
                                    </h3>
                                    <span class="listing-address adr">
                                        <span class="street-address">7000 Grand Ave,</span>
                                        <span class="city-state">
                                            <span class="locality">Gurnee</span>,
                                            <span class="region">IL</span>
                                            <span class="postal-code">60031</span>
                                        </span>
                                    </span>
                                    <span class="phone">(847) 623-7673</span>
                                </div>

                                <span class="biz_graphic">
                                <a href="/" target="_blank"><img alt="" border="0" class="listing-ad-image no-tracks" src="http://i1.ypcdn.com/displaygifResize/rd/S6477965.gif"></a>
                                </span>

                                <ul class="features">
                                    <li class="website-feature">
                                        <a href="/" class="track-visit-website"><span class="raquo">»</span>  Website</a>
                                    </li>
                                    <li class="more_info-feature">
                                        <a href="/" class="track-more-info" rel="nofollow"><span class="raquo">»</span> More Info</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div>
                <div class="sb">
                    <div class="sb-group">
                        <ul class="silver">
                            <li class="clearfix silver tier-50 track-listing vcard">
                                <div class="business-info result">
                                    <span class="mapped"></span>
                                    <h3 class="business-title fn org">
                                        <a href="/" class="url " rel="nofollow" title="South Chicago Dodge">South Chicago Dodge</a>
                                    </h3>
                                    <span class="listing-address adr">
                                    <span class="street-address">
                                    7340 S Western Ave,
                                    </span>
                                    <span class="city-state"><span class="locality">Chicago</span>,
                                    <span class="region">IL</span>
                                    <span class="postal-code">60636</span>
                                    </span>
                                    </span>


                                    <span class="phone">(773) 476-7800</span>
                                </div>
                                <span class="description o_ad_text">
                                The largest Dodge, Chrysler &amp; Jeep dealership in…
                                </span>
                                <ul class="features">
                                    <li class="website-feature">
                                        <a href="/" class="track-visit-website" rel="nofollow" target="_blank" title="South Chicago Dodge Website"><span class="raquo">»</span>  Website</a>
                                    </li>
                                    <li class="coupon-feature">
                                        <a href="/" class="track-coupon coupon-link" rel="nofollow" title="South Chicago Dodge coupon"><span class="raquo">»</span> Coupons</a>
                                    </li>
                                    <li class="print_ad-feature">
                                        <a href="/" class="track-print-ad" rel="nofollow" title="Yellow Pages print advertisement for South Chicago Dodge"><span class="raquo">»</span> YP Ad</a>
                                    </li>
                                    <li class="more_info-feature">
                                        <a href="/" class="track-more-info" rel="nofollow"><span class="raquo">»</span> More Info</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>


        </div>
    </div>

</div>
@endsection