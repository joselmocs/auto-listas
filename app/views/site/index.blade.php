@extends('template')

@section('page_title')
{{ Config::get('server.site.title') }}
@endsection

@section('styles')
{{ HTML::style('/styles/home.css') }}
@endsection

@section('body_class')home_main @endsection

@section('conteudo')
<div class="home index" id="canvas">
    <div class="panel" id="top-nav">
        <div class="change-location">
            <h2>Auto Listas</h2>
        </div>
    </div>

    <div class="clear"></div>

    <div class="ugc-mashup">
        <h2 class="ugc-title">
            <span>Rate these popular businesses</span>
        </h2>

        <div class="ugc-listings">

            <div class="ugc-listing">
                <div class="heading">
                    <h3>
                        <a href="#" title="Giordano's">Giordano's</a>
                    </h3>
                    <p class="address">W Jackson Blvd, Chicago</p>
                </div>
                <div class="photo">
                    <a href="#"><img alt="Photo" src="http://i1.ypcdn.com/blob/0c6f8788daf22d027c61b784d914356de703521b_210x120_crop.jpg?7b6b651"></a>
                </div>
                <div class="reviews">
                    <div class="review clearfix">
                        <div class="user-avatar"><img alt="Yp-user-3" src="http://i1.ypcdn.com/webyp/images/users/yp-user-3.gif?7b6b651"></div>
                        <div class="text">
                            <div class="name">
                                <strong><a href="#">justen.arnold</a></strong>
                            </div>
                            <p class="review-snippet">
                                <a href="#">Of course the food is great but the manager on duty was awesome.…</a>
                            </p>
                        </div>
                    </div>
                    <div class="review clearfix">
                        <div class="user-avatar"><img alt="Anonymous" src="http://i2.ypcdn.com/webyp/images/users/anonymous.png?7b6b651"></div>
                        <p class="review-snippet text">
                            <a href="#">Know this place? Write a brief review!</a>
                        </p>
                    </div>
                </div>
                <div class="stars">
                    <div class="stars-container clearfix">
                        <span class="rate-it">Rate It!</span>
                        <div class="rating write-review-stars">
                            <p class="average rating-3-5">3.5 stars</p>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <a href="#" class="more-like-this">See more like this »</a>
            </div>

            <div class="ugc-listing">
                <div class="heading">
                    <h3>
                        <a href="#" title="Giordano's">Giordano's</a>
                    </h3>
                    <p class="address">W Jackson Blvd, Chicago</p>
                </div>
                <div class="photo">
                    <a href="#"><img alt="Photo" src="http://i1.ypcdn.com/blob/0c6f8788daf22d027c61b784d914356de703521b_210x120_crop.jpg?7b6b651"></a>
                </div>
                <div class="reviews">
                    <div class="review clearfix">
                        <div class="user-avatar"><img alt="Yp-user-3" src="http://i1.ypcdn.com/webyp/images/users/yp-user-3.gif?7b6b651"></div>
                        <div class="text">
                            <div class="name">
                                <strong><a href="#">justen.arnold</a></strong>
                            </div>
                            <p class="review-snippet">
                                <a href="#">Of course the food is great but the manager on duty was awesome.…</a>
                            </p>
                        </div>
                    </div>
                    <div class="review clearfix">
                        <div class="user-avatar"><img alt="Anonymous" src="http://i2.ypcdn.com/webyp/images/users/anonymous.png?7b6b651"></div>
                        <p class="review-snippet text">
                            <a href="#">Know this place? Write a brief review!</a>
                        </p>
                    </div>
                </div>
                <div class="stars">
                    <div class="stars-container clearfix">
                        <span class="rate-it">Rate It!</span>
                        <div class="rating write-review-stars">
                            <p class="average rating-3-5">3.5 stars</p>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <a href="#" class="more-like-this">See more like this »</a>
            </div>

            <div class="ugc-listing">
                <div class="heading">
                    <h3>
                        <a href="#" title="Giordano's">Giordano's</a>
                    </h3>
                    <p class="address">W Jackson Blvd, Chicago</p>
                </div>
                <div class="photo">
                    <a href="#"><img alt="Photo" src="http://i1.ypcdn.com/blob/0c6f8788daf22d027c61b784d914356de703521b_210x120_crop.jpg?7b6b651"></a>
                </div>
                <div class="reviews">
                    <div class="review clearfix">
                        <div class="user-avatar"><img alt="Yp-user-3" src="http://i1.ypcdn.com/webyp/images/users/yp-user-3.gif?7b6b651"></div>
                        <div class="text">
                            <div class="name">
                                <strong><a href="#">justen.arnold</a></strong>
                            </div>
                            <p class="review-snippet">
                                <a href="#">Of course the food is great but the manager on duty was awesome.…</a>
                            </p>
                        </div>
                    </div>
                    <div class="review clearfix">
                        <div class="user-avatar"><img alt="Anonymous" src="http://i2.ypcdn.com/webyp/images/users/anonymous.png?7b6b651"></div>
                        <p class="review-snippet text">
                            <a href="#">Know this place? Write a brief review!</a>
                        </p>
                    </div>
                </div>
                <div class="stars">
                    <div class="stars-container clearfix">
                        <span class="rate-it">Rate It!</span>
                        <div class="rating write-review-stars">
                            <p class="average rating-3-5">3.5 stars</p>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <a href="#" class="more-like-this">See more like this »</a>
            </div>

            <div class="ugc-listing">
                <div class="heading">
                    <h3>
                        <a href="#" title="Giordano's">Giordano's</a>
                    </h3>
                    <p class="address">W Jackson Blvd, Chicago</p>
                </div>
                <div class="photo">
                    <a href="#"><img alt="Photo" src="http://i1.ypcdn.com/blob/0c6f8788daf22d027c61b784d914356de703521b_210x120_crop.jpg?7b6b651"></a>
                </div>
                <div class="reviews">
                    <div class="review clearfix">
                        <div class="user-avatar"><img alt="Yp-user-3" src="http://i1.ypcdn.com/webyp/images/users/yp-user-3.gif?7b6b651"></div>
                        <div class="text">
                            <div class="name">
                                <strong><a href="#">justen.arnold</a></strong>
                            </div>
                            <p class="review-snippet">
                                <a href="#">Of course the food is great but the manager on duty was awesome.…</a>
                            </p>
                        </div>
                    </div>
                    <div class="review clearfix">
                        <div class="user-avatar"><img alt="Anonymous" src="http://i2.ypcdn.com/webyp/images/users/anonymous.png?7b6b651"></div>
                        <p class="review-snippet text">
                            <a href="#">Know this place? Write a brief review!</a>
                        </p>
                    </div>
                </div>
                <div class="stars">
                    <div class="stars-container clearfix">
                        <span class="rate-it">Rate It!</span>
                        <div class="rating write-review-stars">
                            <p class="average rating-3-5">3.5 stars</p>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <a href="#" class="more-like-this">See more like this »</a>
            </div>

            <div class="clear"></div>
        </div>
    </div>

    <div class="interest-based-ad">
        <div class="small-promo">aqui vai anuncio</div>
        <div class="leaderboard">aqui vai anuncio</div>
    </div>

</div>
@endsection