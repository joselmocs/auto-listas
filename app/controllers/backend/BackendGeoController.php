<?php


class BackendGeoController extends BaseController {

    /**
     * Exibe a página de cadastro de Estados
     *
     * @return View
     */
    public function get_list_estate()
    {
        $this->set_context(array(
            'menu_atual' => array('geo', 'estate'),
            'estados' => SearchEstate::orderBy('name')->get()
        ));

        return $this->view_make('admin/geo/estate');
    }

    /**
     * Ativa um estado na listagem de resultados
     *
     * @param $estado
     * @return Redirect
     */
    public function get_estate_activate($estado)
    {
        try {
            $estado = SearchEstate::where('id', '=', $estado)->firstOrFail();
            $estado->active = true;
            $estado->save();
        }
        catch (Exception $e) {
            // Ignoramos qualquer tipo de erro
        }

        return Redirect::back();
    }

    /**
     * Desativa um estado na listagem de resultados
     *
     * @param $estado
     * @return Redirect
     */
    public function get_estate_deactivate($estado)
    {
        try {
            $estado = SearchEstate::where('id', '=', $estado)->firstOrFail();
            $estado->active = false;
            $estado->save();
        }
        catch (Exception $e) {
            // Ignoramos qualquer tipo de erro
        }

        return Redirect::back();
    }

    /**
     * Retorna uma lista de cidades para autocomplete
     *
     * @return Illuminate\Support\Facades\Response
     */
    public function jsonp_list_city()
    {
        $db_cidades = SearchCity::where('estate_id', '=', Input::get('estate_id'))
            ->where('name', 'like', '%'. Input::get('q') .'%')
            ->get();

        $cidades = array();
        foreach ($db_cidades as $cidade) {
            array_push($cidades, array('id' => $cidade->id, 'text' => $cidade->name));
        }

        return Response::json(array(
            'total' => count($cidades),
            'cidades' => $cidades
        ))->setCallback(Input::get('callback'));
    }

    /**
     * Exibe a página de cadastro de Cidades
     *
     * @param null|string $estado
     * @return \Illuminate\View\View
     */
    public function get_list_city($estado = null)
    {
        if ($estado != null) {
            $estado = SearchEstate::where('short', '=', strtoupper($estado))->firstOrFail();

            $this->set_json_context(array(
                'estado' => $estado->toArray()
            ));
        }

        $this->set_context(array(
            'menu_atual' => array('geo', 'city'),
            'estado' => $estado,
            'estados' => SearchEstate::orderBy('name')->get()
        ));

        return $this->view_make('admin/geo/city');
    }

    /**
     * Retorna as cidades com as letras inicial requisitada
     *
     * @param string $estado
     * @return Response
     */
    public function post_json_list_city($estado)
    {
        $input = Input::all();
        $validate = array(
            'letter' => 'required|min:1|max:1|alpha'
        );

        $validator = Validator::make($input, $validate);
        if ($validator->fails()) {
            return Response::json($validator->errors());
        }

        $estado = SearchEstate::where('short', '=', strtoupper($estado))->firstOrFail();

        $db_cidades = SearchCity::where('estate_id', '=', $estado->id)
            ->where('name', 'like', Input::get('letter') . '%')
            ->get();

        $cidades = array();

        foreach ($db_cidades as $cidade) {
            array_push($cidades, array(
                'id'=> $cidade->id,
                'name'=> $cidade->name,
                'slug'=> $cidade->slug,
                'active'=> $cidade->active,
                'qtde_empresas'=> $cidade->qtde_empresas()
            ));
        }

        return Response::json($cidades);
    }

    /**
     * Ativa uma cidade na listagem de resultados
     *
     * @param $city
     * @return void
     */
    public function post_active_city($city)
    {
        try {
            $estado = SearchCity::where('id', '=', $city)->firstOrFail();
            $estado->active = true;
            $estado->save();
        }
        catch (Exception $e) {
            // Ignoramos qualquer tipo de erro
        }
    }

    /**
     * Desativa uma cidade na listagem de resultados
     *
     * @param $city
     * @return void
     */
    public function post_deactive_city($city)
    {
        try {
            $estado = SearchCity::where('id', '=', $city)->firstOrFail();
            $estado->active = false;
            $estado->save();
        }
        catch (Exception $e) {
            // Ignoramos qualquer tipo de erro
        }
    }

    /**
     * Retorna uma lista de bairros para autocomplete
     *
     * @return Illuminate\Support\Facades\Response
     */
    public function jsonp_list_district()
    {
        $db_cidades = SearchDistrict::where('city_id', '=', Input::get('city_id'))
            ->where('name', 'like', '%'. Input::get('q') .'%')
            ->get();

        $cidades = array();
        foreach ($db_cidades as $cidade) {
            array_push($cidades, array('id' => $cidade->id, 'text' => $cidade->name));
        }

        return Response::json(array(
            'total' => count($cidades),
            'bairros' => $cidades
        ))->setCallback(Input::get('callback'));
    }

    /**
     * Exibe a página de pesquisa de bairros
     *
     * @return View
     */
    public function get_search_district()
    {
        $this->set_context(array(
            'menu_atual' => array('geo', 'district'),
            'estados' => SearchEstate::orderBy('name')->get(),
            'filtros' => null,
        ));

        $this->set_json_context(array(
            'estado' => null,
            'cidade' => null
        ));

        return $this->view_make('admin/geo/district');
    }

    /**
     * Exibe a página de resultado de pesquisa de bairros
     *
     * @param $estado
     * @param $cidade
     * @param $bairro
     * @param $status
     * @return View
     */
    public function get_search_district_filtered($estado, $cidade, $bairro, $status)
    {
        $filtros = array(
            'estado'=> $estado,
            'cidade'=> null,
            'bairro' => ($bairro == -1) ? '' : $bairro,
            'status' => $status
        );

        $bairros = SearchDistrict::select('search_district.name', 'search_district.slug', 'search_district.city_id',
                                          'search_district.active');

        // Se status não for citado buscamos todas os bairros, ativados e destivados
        if ($status != -1) {
            $bairros = $bairros->where('active', '=', true ? ($status == 1) : false);
        }

        // Se a cidade for informada, verificamos apenas os bairros da mesma
        if ($cidade != -1) {
            $bairros = $bairros->where('search_district.city_id', '=', $cidade);
        }
        // Se não for informado a cidade, verificamos se o estado é informado
        else if ($estado != -1) {
            $bairros = $bairros
                ->join('search_city', 'search_city.id', '=', 'search_district.city_id')
                ->where('search_city.estate_id', '=', $estado);
        }

        if ($bairro != -1) {
            $bairros->where('search_district.name', 'like', '%'. $bairro . '%');
        }

        // Ordenamos e paginamos o resultado
        $bairros = $bairros->orderBy('search_district.name')->paginate(20);

        if ($cidade != -1) {
            $filtros['cidade'] = SearchCity::find($cidade)->toJson();
        }

        $this->set_context(array(
            'menu_atual' => array('geo', 'district'),
            'estados' => SearchEstate::orderBy('name')->get(),
            'filtros' => $filtros,
            'bairros' => $bairros
        ));

        if ($estado != -1) {
            $this->set_json_context(array(
                'estado' => SearchEstate::find($estado)->toArray()
            ));
        }

        if ($cidade != -1) {
            $this->set_json_context(array(
                'cidade' => SearchCity::find($cidade)->toArray()
            ));
        }

        return $this->view_make('admin/geo/district');
    }

    /**
     * Ativa o bairro
     *
     * @param $bairro
     * @return Redirect
     */
    public function get_district_activate($bairro)
    {
        try {
            $model = SearchDistrict::where('id', '=', $bairro)->firstOrFail();
            $model->active = true;
            $model->save();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            // Ignoramos qualquer tipo de erro
        }

        return Redirect::back();
    }

    /**
     * Desativa o bairro
     *
     * @param $bairro
     * @return Redirect
     */
    public function get_district_deactivate($bairro)
    {
        try {
            $model = SearchDistrict::where('id', '=', $bairro)->firstOrFail();
            $model->active = false;
            $model->save();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            // Ignoramos qualquer tipo de erro
        }

        return Redirect::back();
    }
}
