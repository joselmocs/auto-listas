<?php


class BackendTagController extends BaseController {

    /**
     * Exibe a página de cadastro das etiquetas
     *
     * @return View
     */
    public function any_index()
    {
        $this->set_context(array(
            'menu_atual' => array('business', 'tag'),
            'etiquetas' => SearchTag::all()
        ));

        return $this->view_make('admin/tag/index');
    }

    /**
     * Adiciona uma Tag
     *
     * @return Response
     */
    public function post_json_add()
    {
        $input = Input::all();
        $validate = array(
            'tag' => 'required|min:3|max:64'
        );

        $validator = Validator::make($input, $validate);
        if ($validator->fails()) {
            return Response::json($validator->errors());
        }

        try {
            SearchTag::where('name', '=', Input::get('tag'))
                ->orWhere('slug', '=', SearchCity::generate_slug(Input::get('tag')))
                ->firstOrFail();

            return Response::json(
                array('tag' => array("Já existe uma etiqueta com este nome/slug."))
            );
        }
        catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $new = new SearchTag();
            $new->name = Input::get('tag');
            $new->slug = SearchCity::generate_slug(Input::get('tag'));
            $new->save();
        }

        return Response::json(array('success' => true));
    }

    /**
     * Edita uma Tag
     *
     * @return Response
     */
    public function post_json_edit()
    {
        $input = Input::all();
        $validate = array(
            'id' => 'required|integer',
            'tag' => 'required|min:3|max:64'
        );

        $validator = Validator::make($input, $validate);
        if ($validator->fails()) {
            return Response::json($validator->errors());
        }

        try {
            $tag = SearchTag::where('name', '=', Input::get('tag'))
                ->orWhere('slug', '=', SearchCity::generate_slug(Input::get('tag')))
                ->firstOrFail();

            if ($tag->id == Input::get('id')) {
                throw new Illuminate\Database\Eloquent\ModelNotFoundException;
            }

            return Response::json(
                array('tag' => array("Já existe uma etiqueta com este nome/slug."))
            );
        }
        catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $tag = SearchTag::find(Input::get('id'));
            $tag->name = Input::get('tag');
            $tag->slug = SearchCity::generate_slug(Input::get('tag'));
            $tag->save();
        }

        return Response::json(array('success' => true));
    }

    /**
     * Remove uma Tag
     *
     * @param $tag
     * @return Redirect
     */
    public function get_remove($tag)
    {
        try {
            SearchTagCategory::where('tag_id', '=', $tag)->delete();
            SearchTagBusiness::where('tag_id', '=', $tag)->delete();
            SearchTag::where('id', '=', $tag)->delete();
        }
        catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            //
        }

        return Redirect::to(URL::previous());
    }
}
