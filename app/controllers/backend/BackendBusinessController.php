<?php

class BackendBusinessController extends BaseController {

    /**
     * Exibe a página de pesquisa de empresas
     *
     * @return View
     */
    public function get_search_index()
    {
        $this->set_context(array(
            'menu_atual' => array('business', 'business'),
            'categorias' => SearchCategory::all(),
            'estados' => SearchEstate::all(),
            'filtros' => null,
        ));

        $this->set_json_context(array(
            'estado' => null,
            'cidade' => null,
            'bairro' => null
        ));

        return $this->view_make('admin/business/search');
    }

    /**
     * Exibe a página de resultado de pesquisa de empresas
     *
     * @param $estado
     * @param $cidade
     * @param $bairro
     * @param $categoria
     * @param $status
     * @param $nome
     * @return View
     */
    public function get_search_filtered($estado, $cidade, $bairro, $categoria, $status, $nome)
    {
        $filtros = array(
            'estado'=> $estado,
            'cidade'=> null,
            'bairro'=> null,
            'categoria'=> $categoria,
            'status' => $status,
            'nome' => ($nome == -1) ? '' : $nome
        );

        $empresas = SearchBusiness::select();

        // Se status não for citado buscamos todas as empresas, ativadas e destivadas
        if ($status != -1) {
            $empresas = $empresas->where('active', '=', true ? ($status == 1) : false);
        }

        // Verificamos se o bairro foi informado, se for buscamos apenas as empresas daquele bairro
        if ($bairro != -1) {
            $empresas = $empresas->where('district_id', '=', $bairro);
        }
        // Se não for informado o bairro, verificamos se a cidade é informada
        else if ($cidade != -1) {
            $empresas = $empresas
                ->join('search_district', 'search_district.id', '=', 'search_business.district_id')
                ->join('search_city', 'search_city.id', '=', 'search_district.city_id')
                ->where('search_city.id', '=', $cidade);
        }
        // Se bão for informado a cidade, verificamos se o estado é informado
        else if ($estado != -1) {
            $empresas = $empresas
                ->join('search_district', 'search_district.id', '=', 'search_business.district_id')
                ->join('search_city', 'search_city.id', '=', 'search_district.city_id')
                ->join('search_estate', 'search_estate.id', '=', 'search_city.estate_id')
                ->where('search_estate.id', '=', $estado);
        }

        if ($categoria != -1) {
            if ($categoria == 0) {
                $empresas = $empresas->whereNotIn('id', function($query){
                    $query->distinct()
                        ->select('business_id')
                        ->from('search_business_category');
                });
            }
            else {
                $empresas = $empresas
                    ->join('search_business_category', 'search_business_category.business_id', '=', 'search_business.id')
                    ->where('search_business_category.category_id', '=', $categoria);
            }
        }

        if ($nome != -1) {
            $empresas->where('search_business.name', 'like', '%'. $nome . '%');
        }

        // Ordenamos e paginamos o resultado
        $empresas = $empresas->orderBy('search_business.name')->paginate(20);

        if ($cidade != -1) {
            $filtros['cidade'] = SearchCity::find($cidade)->toJson();
        }

        if ($bairro != -1) {
            $filtros['bairro'] = SearchDistrict::find($bairro)->toJson();
        }

        $this->set_context(array(
            'menu_atual' => array('business', 'business'),
            'categorias' => SearchCategory::all(),
            'estados' => SearchEstate::all(),
            'filtros' => $filtros,
            'empresas' => $empresas
        ));

        if ($estado != -1) {
            $this->set_json_context(array(
                'estado' => SearchEstate::find($estado)->toArray()
            ));
        }

        if ($cidade != -1) {
            $this->set_json_context(array(
                'cidade' => SearchCity::find($cidade)->toArray()
            ));
        }

        if ($bairro != -1) {
            $this->set_json_context(array(
                'bairro' => SearchDistrict::find($bairro)->toArray()
            ));
        }

        return $this->view_make('admin/business/search');
    }

    /**
     * Exibe a página de adição de empresas
     *
     * @return View
     */
    public function get_add()
    {
        $this->set_context(array(
            'menu_atual' => array('business', 'business'),
            'categorias' => SearchCategory::all(),
            'estados' => SearchEstate::all(),
            'mode' => 'new',
        ));

        $this->set_json_context(array(
            'mode' => 'new'
        ));

        return $this->view_make('admin/business/edit');
    }

    /**
     * Exibe a página de adição/edição de empresas
     *
     * @throws Symfony\Component\Process\Exception\RuntimeException
     * @return Response
     */
    public function post_json_save()
    {
        $input = Input::all();
        $validate = array(
            'mode' => 'required|alpha',
            'name' => 'required|min:3|max:128',
            'address' => 'max:256',
            'zipcode' => 'max:8',
            'district_id' => 'required|integer',
            'ddd' => 'max:2',
            'phone' => 'required|min:8|max:12',
            'website' => 'url|max:128',
            'facebook' => 'max:128',
            'twitter' => 'max:128',
            'active' => 'required|integer',
        );

        $validator = Validator::make($input, $validate);
        if ($validator->fails()) {
            return Response::json($validator->errors());
        }

        if (Input::get('mode') == 'new') {
            $business = new SearchBusiness;
        }
        else if (Input::get('mode') == 'edit') {
            $business = SearchBusiness::find(Input::get('id'));
        }
        else {
            throw new \Symfony\Component\Process\Exception\RuntimeException;
        }

        $business->name = Input::get('name');
        $business->address = Input::get('address');
        $business->district_id = Input::get('district_id');
        $business->zipcode = Input::get('zipcode');
        $business->phone = Input::get('ddd') . Input::get('phone');
        $business->website = Input::get('website');
        $business->facebook = Input::get('facebook');
        $business->twitter = Input::get('twitter');
        $business->active = true ? Input::get('active') : false;
        $business->save();

        // Apagamos todas as categorias da empresa antes adicionarmos as novas
        SearchBusinessCategory::where('business_id', '=', $business->id)->delete();

        if (Input::get('categories') != "null") {
            foreach (Input::get('categories') as $category) {
                $catb = new SearchBusinessCategory();
                $catb->category_id = $category;
                $catb->business_id = $business->id;
                $catb->save();
            }
        }

        return Response::json(array(
            'success' => true,
            'id' => $business->id
        ));
    }

    /**
     * Exibe a página de adição de empresas
     *
     * @param int $empresa
     * @return View
     */
    public function get_edit($empresa)
    {
        $empresa = SearchBusiness::find($empresa);

        $this->set_context(array(
            'menu_atual' => array('business', 'business'),
            'categorias' => SearchCategory::all(),
            'estados' => SearchEstate::all(),
            'empresa' => $empresa,
            'mode' => 'edit',
            'ids_categorias' => $empresa->categories_id_list()
        ));

        $this->set_json_context(array(
            'mode' => 'edit',
            'empresa' => $empresa->toArray(),
            'bairro' => $empresa->district->toArray(),
            'cidade' => $empresa->district->city->toArray(),
            'estado' => $empresa->district->city->estate->toArray(),
            'url_previous' => URL::previous()
        ));

        return $this->view_make('admin/business/edit');
    }

    /**
     * Exibe a página de informações da empresa
     *
     * @param int $empresa
     * @return View
     */
    public function get_view($empresa)
    {
        $this->set_context(array(
            'menu_atual' => array('business', 'business'),
            'empresa' => SearchBusiness::find($empresa)
        ));

        return $this->view_make('admin/business/view');
    }
}
