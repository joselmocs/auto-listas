<?php


class BackendUserController extends BaseController {

    /**
     * Exibe a listagem de usuários do sistema
     *
     * @return View
     */
    public function get_users_system()
    {
        $this->set_context(array(
            'menu_atual' => array('user', 'system'),
            'usuarios' => User::all()
        ));

        return $this->view_make('admin/user/system');
    }

}