<?php


class BackendController extends BaseController {

    /**
     * Exibe a página de login da administração
     *
     * @return View
     */
    public function get_index()
    {
        $this->set_context(array(
            'menu_atual' => array('index', null)
        ));

        if (Auth::check()) {
            return $this->view_make('admin/index');
        }

        return $this->view_make('admin/login');
    }

    /**
     * Action que tenta fazer a autenticação do usuário.
     *
     * @return Redirect
     */
    public function post_login()
    {
        $validate = array(
            "username_field" => "required",
            "password_field" => "required"
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            return Redirect::to(URL::previous())->withErrors($validated);
        }

        $error = array();
        try {
            Auth::attempt(array(
                'identifier' => Input::get('username_field'),
                'password' => Input::get('password_field')
            ));
        }
        catch (Exception $e) {
            array_push($error, "Nome de usuário e/ou senha inválidos.");
        }

        if (count($error) > 0) {
            return Redirect::to(URL::previous())->withErrors($error);
        }

        return Redirect::action('BackendController@get_index');
    }

    /**
     * Faz logout do usuário
     *
     * @return Redirect
     */
    public function get_logout()
    {
        Auth::logout();
        return Redirect::action('BackendController@get_index');
    }

}
