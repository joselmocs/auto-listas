<?php


class BackendCategoryController extends BaseController {

    /**
     * Exibe a página de cadastro das categorias
     *
     * @return View
     */
    public function any_index()
    {
        $this->set_context(array(
            'menu_atual' => array('business', 'category'),
            'categorias' => SearchCategory::all(),
            'etiquetas' => SearchTag::all()
        ));

        return $this->view_make('admin/category/index');
    }

    /**
     * Adiciona uma Categoria
     *
     * @return Response
     */
    public function post_json_add()
    {
        $input = Input::all();
        $validate = array(
            'category' => 'required|min:3|max:64'
        );

        $validator = Validator::make($input, $validate);
        if ($validator->fails()) {
            return Response::json($validator->errors());
        }

        try {
            SearchCategory::where('name', '=', Input::get('category'))
                ->orWhere('slug', '=', SearchCity::generate_slug(Input::get('category')))
                ->firstOrFail();

            return Response::json(
                array('category' => array("Já existe uma categoria com este nome/slug."))
            );
        }
        catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $new = new SearchCategory;
            $new->name = Input::get('category');
            $new->slug = SearchCity::generate_slug(Input::get('category'));
            $new->save();

            if (Input::get('tags') != "null") {
                foreach (Input::get('tags') as $tag) {
                    $tagc = new SearchTagCategory;
                    $tagc->category_id = $new->id;
                    $tagc->tag_id = (int) $tag;
                    $tagc->save();
                }
            }
        }

        return Response::json(array('success' => true));
    }

    /**
     * Edita uma Categoria
     *
     * @return Response
     */
    public function post_json_edit()
    {
        $input = Input::all();
        $validate = array(
            'id' => 'required|integer',
            'category' => 'required|min:3|max:128'
        );

        $validator = Validator::make($input, $validate);
        if ($validator->fails()) {
            return Response::json($validator->errors());
        }

        try {
            $tag = SearchCategory::where('name', '=', Input::get('category'))
                ->orWhere('slug', '=', SearchCity::generate_slug(Input::get('category')))
                ->firstOrFail();

            if ($tag->id == Input::get('id')) {
                throw new Illuminate\Database\Eloquent\ModelNotFoundException;
            }

            return Response::json(
                array('category' => array("Já existe uma categoria com este nome/slug."))
            );
        }
        catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $category = SearchCategory::find(Input::get('id'));
            $category->name = Input::get('category');
            $category->slug = SearchCity::generate_slug(Input::get('category'));
            $category->save();

            SearchTagCategory::where('category_id', '=', Input::get('id'))->delete();

            if (Input::get('tags') != "null") {
                foreach (Input::get('tags') as $tag) {
                    $tagc = new SearchTagCategory;
                    $tagc->category_id = $category->id;
                    $tagc->tag_id = (int) $tag;
                    $tagc->save();
                }
            }
        }

        return Response::json(array('success' => true));
    }

    /**
     * Remove uma Categoria
     *
     * @param $category
     * @return Redirect
     */
    public function get_remove($category)
    {
        try {
            SearchTagCategory::where('category_id', '=', $category)->delete();
            SearchBusinessCategory::where('category_id', '=', $category)->delete();
            SearchCategory::where('id', '=', $category)->delete();
        }
        catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            //
        }

        return Redirect::to(URL::previous());
    }
}
