<?php

class UtilController {
    /**
     * Retorna a string transformada em url
     *
     * @param $name
     * @param bool $blank
     * @return string
     */
    public static function generate_slug($name, $blank = false)
    {
        $separator = '-';
        if ($blank) {
            $separator = ' ';
        }
        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $name);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $separator, $clean);

        return $clean;
    }
}
