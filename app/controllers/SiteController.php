<?php

class SiteController extends BaseController {

    /**
     * Exibe a página principal do site
     *
     * @return View
     */
    public function any_index()
    {
        $this->set_context(array(
            'inicial' => true
        ));

        return $this->view_make('site/index');
    }

    public function get_search()
    {
        $this->set_context(array(
            'inicial' => false,
            'resultado' => SearchBusiness::all()
        ));

        return $this->view_make('site/search');
    }

}