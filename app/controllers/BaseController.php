<?php

use Illuminate\Routing\Controllers\Controller;


class BaseController extends Controller {

    /**
     * Context base do backend
     *
     * @var array
     */
    private $context = array(
        // Context com informações enviadas para o template
        'private' => array(),
        // Context transformado em Json para ser usado junto aos scripts
        'json' => array()
    );

    /**
     * Prepara o context para o javascript
     *
     * @param array $contexts
     */
    public function set_json_context($contexts = array())
    {
        foreach ($contexts as $key => $value) {
            $this->context['json'][$key] = $value;
        }
    }

    /**
     * Prepara o context
     *
     * @param array $contexts
     */
    public function set_context($contexts = array())
    {
        foreach ($contexts as $key => $value) {
            $this->context['private'][$key] = $value;
        }
    }

    /**
     * Constrói o template
     *
     * @param string $template
     * @return \Illuminate\View\View
     */
    public function view_make($template)
    {
        return View::make($template)
            ->with('json', json_encode($this->context['json']))
            ->with('context', (object) $this->context['private']);
    }
}
