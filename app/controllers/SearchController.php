<?php

use Illuminate\Routing\Controllers\Controller;


class SearchController extends Controller {

    /**
     * Limite de resultados que pode ser retornado para o autocomplete
     *
     * @var integer
     */
    private $limit = 15;

    /**
     * Retorna uma lista de locais o autocomplete da pagina inicial
     *
     * @return Response
     */
    public function json_search_location()
    {
        $matches = array(
            'searchString' => Input::get('q'),
            'matches' => array()
        );

        foreach (SearchCity::matching(Input::get('q'), $this->limit) as $city) {
            array_push($matches['matches'], array(
                'type' => 'c',
                'matchid' => $city->slug,
                'matchstring' => $city->name .', '. $city->estate->short
            ));
        }

        $diff = $this->limit - count($matches['matches']);

        if ($diff > 0) {
            foreach (SearchDistrict::matching(Input::get('q'), $diff) as $district) {
                array_push($matches['matches'], array(
                    'type' => 'd',
                    'matchid' => $district->slug,
                    'matchstring' => $district->name .', '. $district->city->name .', '. $district->city->estate->short
                ));
            }
        }

        return Response::json($matches);
    }

    /**
     * Retorna uma lista de termos o autocomplete da pagina inicial
     *
     * @return Response
     */
    public function json_search_term()
    {
        $matches = array(
            'searchString' => Input::get('q'),
            'matches' => array()
        );

        foreach (SearchTag::matching(Input::get('q')) as $tag) {
            array_push($matches['matches'], array(
                'type' => 't',
                'matchid' => $tag->slug,
                'matchstring' => $tag->name
            ));
        }

        return Response::json($matches);
    }

}