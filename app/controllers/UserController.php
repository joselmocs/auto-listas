<?php

use Toddish\Verify\Models\Role;

class UserController extends Controller {

    /**
     * Action que tenta fazer a autenticação do usuário.
     *
     * @return Redirect
     */
    public function post_login()
    {
        $validate = array(
            "username" => "required",
            "password" => "required"
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            return Redirect::to(Input::get('actual_page'))->withErrors($validated);
        }

        $error = array();

        try {
            Auth::attempt(array('username' => Input::get('username'), 'password' => Input::get('password')));
        }
        catch (Toddish\Verify\UserNotFoundException $e) {
            array_push($error, "Este usuário não existe.");
        }
        catch (Toddish\Verify\UserPasswordIncorrectException $e) {
            array_push($error, "Senha não corresponde com a conta.");
        }
        catch (Toddish\Verify\UserUnverifiedException $e) {
            array_push($error, "Esta conta ainda não foi verificada.");
        }
        catch (Toddish\Verify\UserDisabledException $e) {
            array_push($error, "Esta conta está atualmente banida.");
        }

        if (count($error) > 0) {
            return Redirect::to(URL::previous())->withErrors($error);
        }

        return Redirect::to('dashboard');
    }

    /**
     * Faz logout do usuário
     *
     * @return void
     */
    public function get_logout()
    {
        Auth::logout();

        return Redirect::to('/');
    }

    /**
     * Exibe a página de cadastro de novo usuário
     *
     * @return View
     */
    public function get_register()
    {
        if (Auth::check()) {
            return Redirect::to('dashboard');
        }

        return View::make('user/register');
    }

    /**
     * Registra um usuário
     *
     * @return Response
     */
    public function post_register()
    {
        $input = Input::all();

        $validate = array(
            'username' => 'required|min:3|max:16|alpha_dash', //|unique:users
            'password' => 'required|min:4|max:16',
            'firstname' => 'required|min:2|max:32',
            'lastname' => 'required|min:2|max:32',
            'email' => 'required|email' //|unique:users
        );

        $validator = Validator::make($input, $validate);

        if ($validator->fails()) {
            return Response::json($validator->errors());
        }

        // Verificamos se o nome de usuário informado está na lista de nomes proibidos
        $lista = SwearList::where('name', '=', strtoupper($input['username']))->get();
        if (count($lista) > 0) {
            return Response::json(array('Não é permitido o registro usando este nome de usuário.'));
        }

        $user = new User;
        $user->username = strtoupper($input['username']);
        $user->email = strtolower($input['email']);
        $user->password = $input['password'];
        $user->save();

        $user->profile->firstname = ucfirst(strtolower($input['firstname']));
        $user->profile->lastname = ucfirst(strtolower($input['lastname']));
        $user->profile->save();

        // Adicionamos o usuário ao seu grupo
        $role = Role::where('name', '=', Config::get('verify::user'))->firstOrFail();
        $user->roles()->sync(array($role->id));

        $this->send_activation_mail($user, $input['password']);

        return Response::json(array(
            'success' => true
        ));
    }

    /**
     * Envia o email de ativação para o usuário
     *
     */
    private function send_activation_mail($user, $password = null)
    {
        $mail_data = array(
            'user' => $user,
            'password' => $password,
            'url' => Config::get('app.url'),
            'site_name' => Config::get('server.site.name'),
        );

        Mail::send('emails.auth.active', $mail_data, function($m) use ($user, $mail_data) {
            $m->to($user->email, $user->profile->firstname)->subject($mail_data['site_name'] .' - Ativação de Conta');
        });
    }

    /**
     * Ativa a conta de um usuário
     *
     * @param $user
     * @param $salt
     * @return View
     */
    public function get_activate($user, $salt)
    {
        if (Auth::check()) {
            return Redirect::to('dashboard');
        }

        $response = null;
        try {
            $user = User::where('username', '=', strtoupper($user))->firstOrFail();

            if ($user->salt != $salt) {
                throw new Exception;
            }
            else {
                if ($user->verified == 1) {
                    $response = "Esta conta já está ativa.";
                }
                else {
                    // Ativa a conta do usuário no site
                    $user->update(array('verified' => 1));
                    $user->save();
                }
            }
        }
        catch (Exception $e) {
            $response = "Usuário ou link de ativação inválido.";
        }

        if ($response === null) {
            $response = "Conta ativada com sucesso!";
        }

        return View::make('user/activate')->with('response', $response);
    }

    /**
     * Exibe a página de recuperacao de conta
     *
     * @return View
     */
    public function get_retrieve()
    {
        if (Auth::check()) {
            return Redirect::to('dashboard');
        }

        return View::make('user/retrieve');
    }

    /**
     * Envia o email de recuperacao de conta
     *
     * @return Response
     */
    public function post_retrieve()
    {
        $input = Input::all();

        if ($input['option'] == "password") {
            $validate = array('informado' => 'required|min:3|max:16|alpha_dash');
        }

        $validator = Validator::make($input, $validate);

        if ($validator->fails()) {
            return Response::json($validator->errors());
        }

        // Recuperação da Senha
        if ($input['option'] == "password") {
            try {
                $user = User::where('username', '=', strtoupper($input['informado']))->firstOrFail();
            }
            catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                return Response::json(array('Nome de usuário não cadastrado.'));
            }

            $mail_data = array(
                'user' => $user,
                'url' => Config::get('app.url'),
                'site_name' => Config::get('server.site.name'),
                'tipo_email' => 'password'
            );

            Mail::send('emails.auth.retrieve', $mail_data, function($m) use ($user, $mail_data) {
                $m->to($user->email, $user->profile->firstname)->subject($mail_data['site_name'] .' - Recuperação de senha');
            });
        }

        // Novo envio do email de ativação da conta
        if ($input['option'] == "activation") {
            try {
                $user = User::where('email', '=', strtoupper($input['informado']))->firstOrFail();
            }
            catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                return Response::json(array('E-mail não cadastrado.'));
            }

            if ($user->verified == 1) {
                return Response::json(array('Esta conta já foi ativada e já pode autenticar no site.'));
            }

            $this->send_activation_mail($user);
        }

        return Response::json(array(
            'success' => true
        ));
    }

    /**
     * Exibe a página para alteração de senha
     *
     */
    public function get_retrieve_password($username, $salt)
    {
        if (Auth::check()) {
            return Redirect::to('dashboard');
        }

        try {
            $user = User::where('username', '=', strtoupper($username))->firstOrFail();
            if ($user->salt != $salt) {
                throw new Exception;
            }
        }
        catch (Exception $e) {
            App::abort(404);
        }

        return View::make('user/retrieve_password')->with(array(
            'username' => $username,
            'salt' => $salt
        ));
    }

    /**
     * Altera a senha do usuário
     *
     */
    public function post_retrieve_password()
    {
        if (Auth::check()) {
            return Redirect::to('dashboard');
        }

        $input = Input::all();

        try {
            $user = User::where('username', '=', strtoupper($input['username']))->firstOrFail();
            if ($user->salt != $input['salt']) {
                throw new Exception;
            }
        }
        catch (Exception $e) {
            App::abort(404);
        }

        $validator = Validator::make($input, array('password' => 'required|min:4|max:16'));
        if ($validator->fails()) {
            return Response::json($validator->errors());
        }

        // Altera a senha do usuário no site
        $user->password = $input['password'];
        $user->save();

        // Envia um email para o usuário comunicando a troca de senha recente em sua conta
        $mail_data = array(
            'user' => $user,
            'url' => Config::get('app.url'),
            'site_name' => Config::get('server.site.name'),
            'tipo_email' => 'password_changed'
        );

        Mail::send('emails.auth.retrieve', $mail_data, function($m) use ($user, $mail_data) {
            $m->to($user->email, $user->profile->firstname)->subject($mail_data['site_name'] .' - Comunicado de troca de senha');
        });

        return Response::json(array(
            'success' => true
        ));
    }

}