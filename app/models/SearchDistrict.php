<?php


class SearchDistrict extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'search_district';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('id', 'name', 'slug', 'search_tag', 'city_id');

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::creating(function($model)
        {
            $slug = $model->name .' '. $model->city->name .' '. $model->city->estate->short;
            $model->slug = UtilController::generate_slug($slug);
            $model->search_tag = $slug;
        });

        static::updating(function($model)
        {
            $slug = $model->name .' '. $model->city->name .' '. $model->city->estate->short;
            $model->slug = UtilController::generate_slug($slug);
            $model->search_tag = $slug;
        });
    }

    /**
     * Retorna a cidade da empresa
     *
     * @var SearchCity
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('SearchCity', 'city_id');
    }

    /**
     * Retorna a quantidade de empresas que estão cadastradas no bairro
     *
     * @return int
     */
    public function qtde_empresas()
    {
        return SearchBusiness::where('district_id', '=', $this->id)->count();
    }

    /**
     * Retorna uma lista de bairros de acordo com o parametro.
     * TODO: Implementar cache.
     *
     * @param $term
     * @param $limit
     * @return array|SearchCity
     */
    public static function matching($term, $limit = 15) {
        return SearchDistrict::select(array('search_district.name', 'search_district.slug', 'search_district.city_id'))
            ->where('search_district.search_tag', 'like', '%'. $term .'%')
            ->join('search_city', 'search_city.id', '=', 'search_district.city_id')
            ->join('search_estate', 'search_estate.id', '=', 'search_city.estate_id')
            ->where('search_estate.active', '=', true)
            ->where('search_city.active', '=', true)
            ->where('search_district.active', '=', true)
            ->take($limit)->orderBy('search_district.name')->get();
    }
}