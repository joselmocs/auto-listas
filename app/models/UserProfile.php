<?php

class UserProfile extends Eloquent {

    /**
     * Database associated with the model
     *
     * @var string
     */
    public $connection = 'autolistas';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users_profile';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('id', 'user_id', 'firstname', 'lastname');

    /**
     * Retorna o Usuario pertencente a este Profile
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

}