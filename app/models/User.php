<?php

class User extends Toddish\Verify\Models\User {

    /**
     * Database associated with the model
     *
     * @var string
     */
    public $connection = 'autolistas';

    /**
     * Retorna o profile do usuario, caso não existe é criado um profile novo.
     *
     * @return UserProfile
     */
    public function profile()
    {
        try {
            UserProfile::where('user_id', '=', $this->id)->firstOrFail();
        }
        catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            UserProfile::create(array(
                'user_id' => $this->id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ));
        }

        return $this->hasOne('UserProfile', 'user_id');
    }

    /**
     * Is the User a Role
     *
     * @param  array|string  $roles A single role or an array of roles
     * @return boolean
     */
    public function is($roles)
    {
        $roles = !is_array($roles)
            ? array($roles)
            : $roles;

        $to_check = $this->getToCheck();

        $valid = false;
        foreach ($to_check->roles as $role)
        {
            if (in_array($role->slug, $roles)) {
                $valid = true;
                break;
            }
        }

        return $valid;
    }

    /**
     * Can the User do something
     *
     * @param  array|string $permissions Single permission or an array or permissions
     * @return boolean
     */
    public function can($permissions)
    {
        $permissions = !is_array($permissions)
            ? array($permissions)
            : $permissions;

        $to_check = $this->getToCheck();

        // Are we a super backend?
        foreach ($to_check->roles as $role)
        {
            if ($role->slug == 'super_admin') {
                return true;
            }
        }

        $valid = false;
        foreach ($to_check->roles as $role)
        {
            foreach ($role->permissions as $permission)
            {
                if (in_array($permission->slug, $permissions)) {
                    $valid = true;
                    break 2;
                }
            }
        }

        return $valid;
    }

    public function group()
    {
        if ($this->level(10, '=')) {
            return "Super Administrador";
        }
        else if ($this->level(5, '>=')) {
            return "Administrador";
        }
        else if ($this->level(3, '=')) {
            return "Empresa";
        }
        else {
            return "Usuário";
        }
    }

}