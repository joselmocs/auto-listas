<?php

class SearchBusiness extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'search_business';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('id', 'name', 'address', 'district_id', 'zipcode', 'phone', 'thumb', 'website',
                                'facebook', 'twitter', 'rating_votes', 'rating_points', 'relevance', 'active');

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::creating(function($model)
        {
            $slug = $model->name;
            $model->slug = UtilController::generate_slug($slug);
        });

        static::updating(function($model)
        {
            $slug = $model->name;
            $model->slug = UtilController::generate_slug($slug);
        });
    }

    /**
     * Retorna o bairro empresa
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function district()
    {
        return $this->belongsTo('SearchDistrict', 'district_id');
    }

    /**
     * Retorna a cidade
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->district->city;
    }

    /**
     * Retorna a cidade
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estate()
    {
        return $this->city()->estate;
    }

    /**
     * Retorna as categorias da empresa
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany('SearchCategory', 'search_business_category', 'business_id', 'category_id');
    }

    /**
     * Retorna os ids das categorias da empresa em um array
     *
     * @return array
     */
    public function categories_id_list()
    {
        $categorias = array();
        foreach ($this->categories as $categoria) {
            array_push($categorias, $categoria->id);
        }
        return $categorias;
    }

    /**
     * Retorna o ddd da empresa
     *
     * @return string
     */
    public function ddd()
    {
        return substr($this->phone, 0, 2);
    }

    /**
     * Retorna o telefone da empresa sem o ddd
     *
     * @return string
     */
    public function telephone()
    {
        if ($this->ddd) {
            return substr($this->phone, 2);
        }
        return $this->phone;
    }
}