<?php

/**
 * @property mixed id
 */
class SearchCity extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'search_city';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('id', 'name', 'slug', 'search_tag', 'estate_id');

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::creating(function($model)
        {
            $slug = $model->name .' '. $model->estate->short;
            $model->slug = UtilController::generate_slug($slug);
            $model->search_tag = $slug;
        });

        static::updating(function($model)
        {
            $slug = $model->name .' '. $model->estate->short;
            $model->slug = UtilController::generate_slug($slug);
            $model->search_tag = $slug;
        });
    }

    /**
     * Retorna o estado da cidade
     *
     * @var SearchCity
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function business()
    {
        return $this->belongsTo('SearchBusiness', 'city_id');
    }

    /**
     * Retorna o estado da cidade
     *
     * @var SearchCity
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estate()
    {
        return $this->belongsTo('SearchEstate', 'estate_id');
    }

    /**
     * Retorna a quantidade de empresas que estão cadastradas na cidade
     *
     * @return int
     */
    public function qtde_empresas()
    {
        return SearchBusiness::join('search_district', 'search_district.city_id', '=', 'search_business.id')
            ->where('search_district.city_id', '=', $this->id)->count();
    }

    /**
     * Retorna uma lista de cidades de acordo com o parametro.
     * TODO: Implementar cache.
     *
     * @param $term
     * @param $limit
     * @return array|SearchCity
     */
    public static function matching($term, $limit = 15) {
        return SearchCity::select(array('search_city.name', 'search_city.slug', 'search_city.estate_id'))
            ->where('search_city.search_tag', 'like', $term .'%')
            ->join('search_estate', 'search_estate.id', '=', 'search_city.estate_id')
            ->where('search_estate.active', '=', true)
            ->where('search_city.active', '=', true)
            ->take($limit)->orderBy('search_city.name')->get();
    }
}