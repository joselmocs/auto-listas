<?php

class SearchEstate extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'search_estate';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('id', 'name', 'slug', 'short');

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::creating(function($model)
        {
            $model->slug = UtilController::generate_slug($model->name);
        });

        static::updating(function($model)
        {
            $model->slug = UtilController::generate_slug($model->name);
        });
    }

    /**
     * Retorna a quantidade de cidades que estão cadastrados no estado
     *
     * @return int
     */
    public function qtde_cidades()
    {
        return SearchCity::where('estate_id', '=', $this->id)->count();
    }
}