<?php

class SearchBusinessCategory extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'search_business_category';

}