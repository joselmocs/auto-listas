<?php

class SearchCategory extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'search_category';

    /**
     * Retorna a lista de tags da categoria
     *
     * @var SearchTag
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany('SearchTag', 'search_tag_category', 'category_id', 'tag_id');
    }
}