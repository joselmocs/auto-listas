<?php

class SearchTag extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'search_tag';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('id', 'name', 'slug', 'relevance');

    /**
     * Retorna uma lista de etiquetas de acordo com o parametro.
     * TODO: Implementar cache.
     *
     * @param $term
     * @return array|SearchTag
     */
    public static function matching($term) {
        return SearchTag::where('name', 'like', '%'. $term .'%')->get();
    }
}